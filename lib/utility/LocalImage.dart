class LocalImage {
  static String logo1 = "assets/images/logo_1.png";
  static String logo2 = "assets/images/logo_2.png";
  static String logo3 = "assets/images/logo_3.gif";

  static String appBackground = "assets/images/icon_bg_white.png";

  static String iconGoogle = "assets/images/icon_google.png";
  static String iconFacebook = "assets/images/icon_facebook.png";
  static String iconTutorial1 = "assets/images/icon_tutorial_1.png";
  static String iconTutorial2 = "assets/images/icon_tutorial_2.png";
  static String iconApple = "assets/images/icon_apple.png";
  static String iconBGWhite = "assets/images/icon_bg_white.png";
  static String iconBgRGB1 = "assets/images/icon_bg_rgb_1.jpg";
  static String iconLogout = "assets/images/icon_logout.png";
  static String iconSmile = "assets/images/icon_smile.png";
  static String iconSad = "assets/images/icon_sad.png";
  static String iconUser = "assets/images/icon_user.png";
  static String iconLocation = "assets/images/icon_location.png";
  static String iconCoffie = "assets/images/icon_coffie.png";
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mode_on/model/EventModel.dart';

import 'FirebaseApi.dart';

class EventsApi extends FirebaseApi {
  EventsApi() : super("mode-on-events");

  Future<List<EventModel>> getData(String userId) async {
    List<EventModel> events = new List<EventModel>();
    QuerySnapshot data = await ref.where('user', isEqualTo: userId).getDocuments();
    if (data.documents.length > 0) {
      for (int x = 0; x < data.documents.length; x++) {
        String id = data.documents[x].reference.path.split("/")[data.documents[x].reference.path.split("/").length - 1];
        events.add(EventModel.fromMap(data.documents[x].data, id));
      }
    }
    return events;
  }

  Future<List<EventModel>> getDataNRow(String userId, int row) async {
    List<EventModel> events = new List<EventModel>();
    QuerySnapshot data = await ref.where('user', isEqualTo: userId).getDocuments();
    if (data.documents.length > 0) {
      int length = data.documents.length > (row - 1) ? row : data.documents.length;
      for (int x = 0; x < length; x++) {
        String id = data.documents[x].reference.path.split("/")[data.documents[x].reference.path.split("/").length - 1];
        events.add(EventModel.fromMap(data.documents[x].data, id));
      }
    }
    return events;
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';

class EventModel {
  String id;
  String name = "";
  String icon = "app";
  int theme = 0;
  String iconType = "icon_data";
  String themeColor1 = "FFEA5186";
  String themeColor2 = "B3EA5186";
  String user = "";
  Timestamp created = Timestamp.now();

  EventModel({this.id, this.name, this.icon, this.iconType, this.theme, this.themeColor1, this.themeColor2, this.user, this.created});

  EventModel.fromMap(Map snapshot, String id)
      : id = id ?? '',
        name = snapshot['name'] ?? "",
        icon = snapshot['icon'] ?? "",
        iconType = snapshot['iconType'] ?? "",
        theme = snapshot['theme'] ?? 0,
        themeColor1 = snapshot['themeColor1'] ?? "",
        themeColor2 = snapshot['themeColor2'] ?? "",
        user = snapshot['user'] ?? "",
        created = snapshot['created'] ?? null;

  toJson() {
    return {
      "name": name,
      "icon": icon,
      "iconType": iconType,
      "theme": theme,
      "themeColor1": themeColor1,
      "themeColor2": themeColor2,
      "user": user,
      "created": created,
    };
  }
}

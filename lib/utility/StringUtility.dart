import 'dart:math';

class StringUtility {
  static String greeting() {
    var hours = DateTime.now().hour;
    if (hours >= 1 && hours <= 12) {
      return 'Morning';
    } else if (hours >= 12 && hours <= 16) {
      return 'Afternoon';
    } else if (hours >= 16 && hours <= 21) {
      return 'Evening';
    } else if (hours >= 21 && hours <= 24) {
      return 'Night';
    } else {
      return 'Night';
    }
  }

  static String generateRandomHexColor() {
    int length = 6;
    String chars = '0123456789ABCDEF';
    String hex = '#';
    while (length-- > 0) hex += chars[(new Random().nextInt(16)) | 0];
    return hex;
  }

  static String getSvgForEvent(int item, String cottomColor, String topColor) {
    String svg = "";
    if (item == 1) {
      svg = """
        <svg width="155" height="156" viewBox="0 0 155 156" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M155 30.3401V33.6901C150.39 36.5701 142.74 43.5001 135.38 49.6901C125.67 57.8701 98.84 83.1701 85.04 83.1701C71.24 83.1701 58.64 59.2701 53.37 51.5001C48.1 43.7301 46.19 36.9101 33.16 36.9101C20.13 36.9101 19.62 46.0101 0 51.9101V30.3401C0 26.4004 0.775974 22.4994 2.28362 18.8596C3.79126 15.2198 6.00103 11.9126 8.78679 9.12688C11.5725 6.34113 14.8797 4.13134 18.5195 2.6237C22.1593 1.11606 26.0603 0.340088 30 0.340088L125 0.340088C132.956 0.340088 140.587 3.50079 146.213 9.12688C151.839 14.753 155 22.3836 155 30.3401Z" fill="topColor"/>
          <path d="M155 33.6901V125.34C155 133.297 151.839 140.927 146.213 146.553C140.587 152.179 132.956 155.34 125 155.34H30C22.0435 155.34 14.4129 152.179 8.78679 146.553C3.1607 140.927 0 133.297 0 125.34V51.8901C19.62 46.0101 20.19 36.8901 33.16 36.8901C46.13 36.8901 48.1 43.7101 53.37 51.4801C58.64 59.2501 71.24 83.1701 85 83.1701C98.76 83.1701 125.63 57.8701 135.34 49.6901C142.74 43.5001 150.39 36.5701 155 33.6901Z" fill="cottomColor"/>
          <defs>
            <linearGradient id="paint0_linear" x1="82" y1="84.8401" x2="148.5" y2="9.34008" gradientUnits="userSpaceOnUse">
              <stop stop-color="topColor"/>
              <stop offset="1" stop-color="cottomColor"/>
            </linearGradient>
            <linearGradient id="paint1_linear" x1="54.5" y1="136.34" x2="114.855" y2="91.9233" gradientUnits="userSpaceOnUse">
              <stop stop-color="topColor"/>
              <stop offset="1" stop-color="cottomColor"/>
            </linearGradient>
          </defs>
        </svg> 
        """;
    } else if (item == 2) {
      svg = """
        <svg width="155" height="156" viewBox="0 0 155 156" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M155 98.75V125.34C155 133.297 151.839 140.927 146.213 146.553C140.587 152.179 132.957 155.34 125 155.34H30C22.0435 155.34 14.4129 152.179 8.78683 146.553C3.16074 140.927 3.02997e-05 133.297 3.02997e-05 125.34V30.34C-0.00896075 24.0321 1.98325 17.8839 5.69003 12.78C13.55 54.71 22.69 63.3 54.05 52C86.91 40.16 90.29 110 129.69 110C145.2 109.98 155 98.75 155 98.75Z" fill="topColor"/>
          <path d="M155 30.3401V98.7501C155 98.7501 145.2 109.98 129.69 109.98C90.2899 109.98 86.9099 40.1601 54.0499 51.9801C22.6899 63.3401 13.5499 54.7101 5.68994 12.7801C6.62301 11.4638 7.67041 10.2323 8.81994 9.1001C11.601 6.31916 14.9032 4.11399 18.5376 2.61084C22.1719 1.10768 26.067 0.336046 29.9999 0.340104H125C132.956 0.340104 140.587 3.50081 146.213 9.1269C151.839 14.753 155 22.3836 155 30.3401Z" fill="cottomColor"/>
          <defs>
            <linearGradient id="paint0_linear" x1="90" y1="71.3401" x2="5.77312" y2="136.222" gradientUnits="userSpaceOnUse">
              <stop stop-color="topColor"/>
              <stop offset="1" stop-color="cottomColor"/>
            </linearGradient>
            <linearGradient id="paint1_linear" x1="152.5" y1="-7.65991" x2="82" y2="64.8401" gradientUnits="userSpaceOnUse">
              <stop stop-color="topColor"/>
              <stop offset="1" stop-color="cottomColor"/>
            </linearGradient>
          </defs>
        </svg> 
        """;
    } else if (item == 3) {
      svg = """
        <svg width="170" height="171" viewBox="0 0 170 171" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M170 113.582V137.437C170 146.163 166.533 154.532 160.363 160.703C154.192 166.873 145.823 170.34 137.097 170.34H32.9032C24.1767 170.34 15.8077 166.873 9.63714 160.703C3.46659 154.532 0 146.163 0 137.437V66.7607C19.8187 54.1697 25.2258 54.0381 43.4871 86.3052C63.2839 121.216 97.92 45.0665 126.842 45.3407C151.783 45.582 159.471 85.8885 170 113.582Z" fill="topColor"/>
          <path d="M170 33.2433V113.582C159.471 85.8885 151.783 45.582 126.787 45.3078C97.92 45.0336 63.2838 121.183 43.4322 86.2723C25.2258 54.082 19.8187 54.1698 0 66.7607V33.2433C0 24.5168 3.46659 16.1478 9.63714 9.97722C15.8077 3.80667 24.1767 0.340088 32.9032 0.340088L137.097 0.340088C145.823 0.340088 154.192 3.80667 160.363 9.97722C166.533 16.1478 170 24.5168 170 33.2433Z" fill="cottomColor"/>
          <defs>
            <linearGradient id="paint0_linear" x1="37.2903" y1="170.34" x2="146.39" y2="51.9901" gradientUnits="userSpaceOnUse">
              <stop stop-color="topColor"/>
              <stop offset="1" stop-color="cottomColor"/>
            </linearGradient>
            <linearGradient id="paint1_linear" x1="102" y1="91.3723" x2="148.742" y2="-1.54299" gradientUnits="userSpaceOnUse">
              <stop stop-color="topColor"/>
              <stop offset="1" stop-color="cottomColor"/>
            </linearGradient>
          </defs>
        </svg> 
        """;
    } else if (item == 4) {
      svg = """
        <svg width="170" height="171" viewBox="0 0 170 171" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M170 107.55V137.437C170 146.163 166.533 154.532 160.363 160.703C154.192 166.873 145.823 170.34 137.097 170.34H32.9032C24.1767 170.34 15.8077 166.873 9.63713 160.703C3.46658 154.532 0 146.163 0 137.437V33.2434C0.000586874 27.0098 1.77192 20.9046 5.10779 15.6386C8.44365 10.3727 13.2068 6.16273 18.8426 3.4989C33.8903 23.7344 37.4329 69.6672 48.3567 85.2853C59.9057 101.737 67.3309 95.1562 96.187 92.6995C118.046 91.4711 108.559 137.667 137.864 133.521C148.722 131.141 155.566 118.265 170 107.55Z" fill="topColor"/>
          <path d="M170 33.2434V107.55C155.566 118.265 148.723 131.141 137.843 133.521C108.559 137.645 118.046 91.4711 96.1652 92.6995C67.3091 95.1782 59.884 101.77 48.335 85.2853C37.3672 69.6672 33.8685 23.7344 18.8208 3.49891C23.2199 1.40661 28.0321 0.327236 32.9034 0.340203H137.097C145.823 0.340203 154.192 3.80678 160.363 9.97732C166.533 16.1479 170 24.5169 170 33.2434Z" fill="cottomColor"/>
          <defs>
            <linearGradient id="paint0_linear" x1="10.4193" y1="170.34" x2="76.3512" y2="101.387" gradientUnits="userSpaceOnUse">
              <stop stop-color="topColor"/>
              <stop offset="1" stop-color="cottomColor"/>
            </linearGradient>
            <linearGradient id="paint1_linear" x1="106.387" y1="88.0819" x2="160.821" y2="1.68593" gradientUnits="userSpaceOnUse">
              <stop stop-color="topColor"/>
              <stop offset="1" stop-color="cottomColor"/>
            </linearGradient>
          </defs>
        </svg> 
        """;
    } else {
      svg = """
        <svg width="155" height="156" viewBox="0 0 155 156" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M155 30.3401V33.6901C150.39 36.5701 142.74 43.5001 135.38 49.6901C125.67 57.8701 98.84 83.1701 85.04 83.1701C71.24 83.1701 58.64 59.2701 53.37 51.5001C48.1 43.7301 46.19 36.9101 33.16 36.9101C20.13 36.9101 19.62 46.0101 0 51.9101V30.3401C0 26.4004 0.775974 22.4994 2.28362 18.8596C3.79126 15.2198 6.00103 11.9126 8.78679 9.12688C11.5725 6.34113 14.8797 4.13134 18.5195 2.6237C22.1593 1.11606 26.0603 0.340088 30 0.340088L125 0.340088C132.956 0.340088 140.587 3.50079 146.213 9.12688C151.839 14.753 155 22.3836 155 30.3401Z" fill="topColor"/>
          <path d="M155 33.6901V125.34C155 133.297 151.839 140.927 146.213 146.553C140.587 152.179 132.956 155.34 125 155.34H30C22.0435 155.34 14.4129 152.179 8.78679 146.553C3.1607 140.927 0 133.297 0 125.34V51.8901C19.62 46.0101 20.19 36.8901 33.16 36.8901C46.13 36.8901 48.1 43.7101 53.37 51.4801C58.64 59.2501 71.24 83.1701 85 83.1701C98.76 83.1701 125.63 57.8701 135.34 49.6901C142.74 43.5001 150.39 36.5701 155 33.6901Z" fill="cottomColor"/>
          <defs>
            <linearGradient id="paint0_linear" x1="82" y1="84.8401" x2="148.5" y2="9.34008" gradientUnits="userSpaceOnUse">
              <stop stop-color="topColor"/>
              <stop offset="1" stop-color="cottomColor"/>
            </linearGradient>
            <linearGradient id="paint1_linear" x1="54.5" y1="136.34" x2="114.855" y2="91.9233" gradientUnits="userSpaceOnUse">
              <stop stop-color="topColor"/>
              <stop offset="1" stop-color="cottomColor"/>
            </linearGradient>
          </defs>
        </svg> 
        """;
    }
    svg = svg.replaceAll("cottomColor", cottomColor);
    svg = svg.replaceAll("topColor", topColor);
    return svg;
  }

  static String dateToDayName(DateTime date) {
    if (date.weekday == 1) {
      return "Monday";
    } else if (date.weekday == 2) {
      return "Tuesday";
    } else if (date.weekday == 3) {
      return "Wednesday";
    } else if (date.weekday == 4) {
      return "Thursday";
    } else if (date.weekday == 5) {
      return "Friday ";
    } else if (date.weekday == 6) {
      return "Saturday";
    } else if (date.weekday == 7) {
      return "Sunday";
    } else {
      return "";
    }
  }

  static String humanDate(DateTime date) {
    if (date != null) {
      List<String> dd = (date.toString().split(" ")[0]).trim().split("-");
      String human = dd[2];
      if (dd[1] == "01") {
        human += " January, " + dd[0];
      } else if (dd[1] == "02") {
        human += " February, " + dd[0];
      } else if (dd[1] == "03") {
        human += " March, " + dd[0];
      } else if (dd[1] == "04") {
        human += " April, " + dd[0];
      } else if (dd[1] == "05") {
        human += " May, " + dd[0];
      } else if (dd[1] == "06") {
        human += " June, " + dd[0];
      } else if (dd[1] == "07") {
        human += " July, " + dd[0];
      } else if (dd[1] == "08") {
        human += " August, " + dd[0];
      } else if (dd[1] == "09") {
        human += " September, " + dd[0];
      } else if (dd[1] == "10") {
        human += " October, " + dd[0];
      } else if (dd[1] == "11") {
        human += " November, " + dd[0];
      } else if (dd[1] == "12") {
        human += " December, " + dd[0];
      }
      return human;
    } else {
      return "";
    }
  }
}

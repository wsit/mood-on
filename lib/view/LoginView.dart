import 'dart:io' show Platform;

import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mode_on/presenter/LoginPresenter.dart';
import 'package:mode_on/utility/StaticData.dart';
import 'package:mode_on/view/HomeView.dart';

class LoginView extends StatefulWidget {
  @override
  LoginAppState createState() => new LoginAppState();
}

class LoginAppState extends State<LoginView> implements LoginAppView {
  LoginAppPresenter _loginAppPresenter;
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool _viewLoading = false;

  LoginAppState() {
    _loginAppPresenter = new LoginAppPresenter(this);
  }

  @override
  void initState() {
    if (Platform.isIOS) {
      AppleSignIn.onCredentialRevoked.listen((_) {
        print("Credentials revoked");
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/icon_bg_white.png"),
            fit: BoxFit.cover,
          ),
        ),
      ),
      Scaffold(
        backgroundColor: Colors.transparent,
        body: new Center(
          child: new ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.all(60),
                    height: MediaQuery.of(context).size.height * 0.60,
                    color: StaticData.BackgroundColor,
                    child: new Center(child: new Image.asset('assets/images/logo_1.png', width: 200)),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30, left: 30, right: 30),
                    color: Colors.transparent,
                    width: MediaQuery.of(context).size.width,
                    height: 70,
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width,
                          decoration: new BoxDecoration(color: Color(0xffffffff), borderRadius: new BorderRadius.all(Radius.circular(10))),
                          child: FlatButton(
                            shape: RoundedRectangleBorder(side: BorderSide(color: Colors.white, width: 4), borderRadius: BorderRadius.circular(300)),
                            splashColor: Colors.white,
                            onPressed: () {},
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  AppleSignInButton(
                                    type: ButtonType.continueButton,
                                    onPressed: () {
                                      _loginAppPresenter.signInWithApple();
                                    }, //_loginAppPresenter.signInWithApple(),//appleLogIn
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 15, left: 30, right: 30),
                    color: Colors.transparent,
                    width: MediaQuery.of(context).size.width,
                    height: 70,
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width,
                          decoration: new BoxDecoration(color: Color(0xffffffff), borderRadius: new BorderRadius.all(Radius.circular(15))),
                          child: FlatButton(
                            splashColor: Colors.white,
                            onPressed: () {
                              _loginAppPresenter.signInWithGoogle();
                            },
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Align(alignment: Alignment.centerLeft, child: Image(image: AssetImage("assets/images/icon_google.png"), height: 23.0)),
                                  Padding(padding: const EdgeInsets.only(left: 10), child: Text('Sign in with Google', style: TextStyle(fontSize: 20, color: Colors.black)))
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      )
    ]);
  }

  @override
  void toastMessage(String message) {
    Fluttertoast.showToast(msg: message, toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM);
  }

  @override
  void isValidUser(bool isValid) {
    if (isValid) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => HomeView()));
    } else {
      toastMessage("Something went wrong");
    }
    setState(() => _viewLoading = false);
  }

  @override
  void loading(bool isLoading) {
    setState(() => _viewLoading = isLoading);
  }
}

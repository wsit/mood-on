import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mode_on/data/UsersApi.dart';
import 'package:mode_on/model/UsersModel.dart';
import 'package:mode_on/utility/LocalImage.dart';
import 'package:mode_on/utility/StaticData.dart';
import 'package:mode_on/view/HomeView.dart';
import 'package:mode_on/view/LoginView.dart';
import 'package:mode_on/view/TutorialView.dart';

class SplashView extends StatefulWidget {
  @override
  SplashAppState createState() => new SplashAppState();
}

class SplashAppState extends State<SplashView> with SingleTickerProviderStateMixin {
  bool visible = true;
  AnimationController animationController;
  Animation<double> animation;

  void startTime() async {
    new Timer(new Duration(seconds: 2), isValidUser);
  }

  void isValidUser() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    if (user == null) {
      String temp = await StaticData.getCookiesSting("skip_tutorial");
      if (temp != null && temp != "true") {
        Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute<Null>(builder: (context) => LoginView()), (Route<dynamic> route) => false);
      } else {
        Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute<Null>(builder: (context) => LoginView()), (Route<dynamic> route) => false);
      }
    } else {
      UsersModel usersModel = await new UsersApi().getUserInfo(user.uid);
      if (usersModel != null) {
        StaticData.CURRENT_USER = usersModel;
        Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute<Null>(builder: (context) => HomeView()), (Route<dynamic> route) => false);
      } else {
        Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute<Null>(builder: (context) => LoginView()), (Route<dynamic> route) => false);
      }
    }
  }

  @override
  void initState() {
    super.initState();
    animationController = new AnimationController(vsync: this, duration: new Duration(seconds: 2));
    animation = new CurvedAnimation(parent: animationController, curve: Curves.easeOut);

    animation.addListener(() => this.setState(() {}));
    animationController.forward();

    setState(() {
      visible = !visible;
    });

    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(LocalImage.appBackground),
            fit: BoxFit.cover,
          ),
        ),
      ),
      Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Image(
                  image: new AssetImage(LocalImage.logo3),
                  width: animation.value * 200,
                  height: animation.value * 200,
                ),
              ],
            ),
          ],
        ),
      )
    ]);
  }
}

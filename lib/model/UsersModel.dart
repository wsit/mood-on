import 'dart:ui';

class UsersModel {
  String id;
  String fname;
  String lname;
  String email;
  String image;
  String phone;
  String address;
  String gender;
  String dob;

  UsersModel({this.id, this.fname, this.lname, this.email, this.image, this.phone, this.address, this.gender, this.dob});

  UsersModel.fromMap(Map snapshot, String id)
      : id = id ?? '',
        fname = snapshot['fname'] ?? "",
        lname = snapshot['lname'] ?? "",
        email = snapshot['email'] ?? "",
        image = snapshot['image'] ?? "",
        phone = snapshot['phone'] ?? "",
        address = snapshot['address'] ?? "",
        gender = snapshot['gender'] ?? "",
        dob = snapshot['dob'] ?? "";

  toJson() {
    return {
      "fname": fname,
      "lname": lname,
      "email": email,
      "image": image,
      "phone": phone,
      "address": address,
      "gender": gender,
      "dob": dob,
    };
  }

  DateTime getDoB() {
    if (dob != null && dob.trim() != "null" && dob.trim() != "") {
      return DateTime.parse(dob.toString());
    } else {
      return null;
    }
  }
}

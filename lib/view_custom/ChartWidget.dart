import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mode_on/model/SurveyRate.dart';

class ChartWidget extends StatefulWidget {
  //final double height;
  final List<SurveyRate> surveyRate;

  const ChartWidget({
    Key key,
    this.surveyRate,
  }) : super(key: key);

  @override
  ChartWidgetState createState() => new ChartWidgetState(200, surveyRate);
}

class ChartWidgetState extends State<ChartWidget> {
  double height;
  List<SurveyRate> surveyRate = new List<SurveyRate>();

  ChartWidgetState(double _height, List<SurveyRate> _surveyRate) {
    height = _height;
    surveyRate = _surveyRate;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(children: <Widget>[
            for (SurveyRate sRate in surveyRate)
              Container(
                margin: EdgeInsets.only(left: 10),
                child: Column(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Container(height: height, width: 50, decoration: new BoxDecoration(color: Color(0xff2F3A5C), borderRadius: new BorderRadius.all(Radius.circular(5)))),
                        Positioned.fill(
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                                height: sRate.value.toDouble(),
                                width: 50,
                                decoration: new BoxDecoration(color: sRate.color, borderRadius: new BorderRadius.all(Radius.circular(5)))),
                          ),
                        ),
                      ],
                    ),
                    Text(sRate.key, style: TextStyle(color: Color(0xff48609E)))
                  ],
                ),
              ),
          ]),
        )
      ],
    );
  }
}

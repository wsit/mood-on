import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mode_on/utility/LocalImage.dart';
import 'package:mode_on/view/AddSurvey.dart';
import 'package:mode_on/view/EventsView.dart';
import 'package:mode_on/view/HomeView.dart';
import 'package:mode_on/view/ProfileView.dart';
import 'package:mode_on/view_custom/CView.dart';
import 'package:mode_on/view_custom/ZoomScaffold.dart';
import 'package:provider/provider.dart';

class NavLayout extends StatelessWidget {
  final List<NavLayoutItem> options = [
    NavLayoutItem(1, Icon(Icons.home, color: Colors.white, size: 24), 'Home'),
    NavLayoutItem(2, Image.asset(LocalImage.iconCoffie, color: Colors.white, height: 22), 'Events'),
    //NavLayoutItem(3, Icon(Icons.add_circle_outline, color: Colors.white, size: 24), 'Add Survey'),
    NavLayoutItem(4, Image.asset(LocalImage.iconUser, color: Colors.white, height: 22), 'Profile'),
    NavLayoutItem(5, Image.asset(LocalImage.iconLogout, color: Colors.white, height: 22), 'Logout'),
  ];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanUpdate: (details) {
        if (details.delta.dx < -6) {
          Provider.of<MenuController>(context, listen: true).toggle();
        }
      },
      child: Container(
        padding: EdgeInsets.only(top: 62, left: 10, bottom: 8, right: MediaQuery.of(context).size.width / 2.9),
        color: Color(0xff03B8C4),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[Padding(padding: const EdgeInsets.only(right: 16))],
            ),
            Spacer(),
            Column(
              children: options.map((item) {
                return InkWell(
                  child: ListTile(
                    leading: item.icon,
                    title: Text(item.title, style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white)),
                  ),
                  onTap: () {
                    if (item.index == 1) {
                      Provider.of<MenuController>(context, listen: true).toggle();
                      Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute<Null>(builder: (context) => HomeView()), (Route<dynamic> route) => false);
                    } else if (item.index == 2) {
                      Provider.of<MenuController>(context, listen: true).toggle();
                      Navigator.of(context).push(new CupertinoPageRoute<Null>(builder: (BuildContext context) => new EventsView()));
                    } else if (item.index == 3) {
                      Provider.of<MenuController>(context, listen: true).toggle();
                      Navigator.of(context).push(new CupertinoPageRoute<Null>(builder: (BuildContext context) => new AddSurveyView()));
                    } else if (item.index == 4) {
                      Provider.of<MenuController>(context, listen: true).toggle();
                      Navigator.of(context).push(new CupertinoPageRoute<Null>(builder: (BuildContext context) => new ProfileView()));
                    } else if (item.index == 5) {
                      Provider.of<MenuController>(context, listen: true).toggle();
                      CView.logout(context);
                    }
                  },
                );
              }).toList(),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}

class NavLayoutItem {
  int index;
  String title;
  Widget icon;

  NavLayoutItem(this.index, this.icon, this.title);
}

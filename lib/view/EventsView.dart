import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mode_on/model/EventModel.dart';
import 'package:mode_on/presenter/EventsViewPresenter.dart';
import 'package:mode_on/utility/StaticData.dart';
import 'package:mode_on/utility/StringUtility.dart';
import 'package:mode_on/view_custom/CView.dart';
import 'package:mode_on/view_custom/NavLayout.dart';
import 'package:mode_on/view_custom/ZoomScaffold.dart';
import 'package:provider/provider.dart';

import 'AddEventsView.dart';
import 'EventDetailes.dart';

class EventsView extends StatefulWidget {
  @override
  EventsViewState createState() => new EventsViewState();
}

class EventsViewState extends State<EventsView> with TickerProviderStateMixin, EventsViewPresenterView {
  MenuController menuController;
  EventsViewPresenter _eventsViewPresenter;
  List<EventModel> eventModels = new List<EventModel>();

  @override
  void initState() {
    this.menuController = new MenuController(vsync: this);
    super.initState();
    this._eventsViewPresenter = new EventsViewPresenter(this);
    this._eventsViewPresenter.getAllByUser(StaticData.CURRENT_USER.id);
  }

  @override
  void dispose() {
    //menuController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: (context) => this.menuController,
      child: ZoomScaffold(
        menuScreen: NavLayout(),
        contentScreen: Layout(
          addBarAction: (cc) => Container(
            padding: EdgeInsets.all(10),
            child: FlatButton(
              color: Color(0xff19DCB9),
              textColor: Colors.white,
              disabledColor: Colors.grey,
              disabledTextColor: Colors.black,
              splashColor: Colors.blueAccent,
              onPressed: () {
                Navigator.of(context).push(new CupertinoPageRoute<Null>(builder: (BuildContext context) => new AddEventsView()));
              },
              child: Text("+ Add", style: TextStyle(fontSize: 16)),
            ),
          ),
          contentBuilder: (cc) => Container(
            child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Colors.transparent,
              child: SingleChildScrollView(
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: new Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                      Align(alignment: Alignment.center, child: Text("Your", textAlign: TextAlign.center, style: TextStyle(fontSize: 26, color: Color(0xff21233A), fontFamily: 'AirbnbCerealBold'))),
                      Align(alignment: Alignment.center, child: Text("Events", textAlign: TextAlign.center, style: TextStyle(fontSize: 26, color: Color(0xff21233A), fontFamily: 'AirbnbCerealBold'))),
                      Column(
                        children: [
                          if (eventModels != null && eventModels.length == 0)
                            Container(
                              padding: EdgeInsets.only(top: 200),
                              child: Align(
                                alignment: Alignment.center,
                                child: Text("Nothing yet to show", textAlign: TextAlign.center, style: TextStyle(fontSize: 20, color: Colors.grey, fontFamily: 'AirbnbCerealBold')),
                              ),
                            ),
                          if (eventModels != null && eventModels.length > 0)
                            for (int x = 0; x < eventModels.length; x = x + 2)
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  InkWell(
                                    child: Container(
                                      padding: EdgeInsets.only(top: ((x + 1 == eventModels.length) ? 30 : 0)),
                                      child: Stack(
                                        children: [
                                          SvgPicture.string(StringUtility.getSvgForEvent(eventModels[x].theme, eventModels[x].themeColor1, eventModels[x].themeColor2), height: 150),
                                          Positioned.fill(
                                            child: Align(
                                              alignment: Alignment.bottomLeft,
                                              child: Container(
                                                padding: EdgeInsets.all(10),
                                                child: Text(eventModels[x].name, style: TextStyle(fontSize: 19, color: Colors.white, fontFamily: 'AirbnbCerealBlack')),
                                              ),
                                            ),
                                          ),
                                          Positioned.fill(
                                            child: Align(
                                              alignment: Alignment.topRight,
                                              child: Container(
                                                padding: EdgeInsets.all(15),
                                                child: CView.getEventIcon(eventModels[x].icon, eventModels[x].iconType),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    onTap: () {
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => EventDetailesView(eventModel: eventModels[x])));
                                    },
                                  ),
                                  Padding(padding: EdgeInsets.only(left: ((x + 1 == eventModels.length) ? 0 : 10), right: ((x + 1 == eventModels.length) ? 0 : 10))),
                                  if ((x + 1) < eventModels.length)
                                    InkWell(
                                      child: Container(
                                        padding: EdgeInsets.only(top: 30),
                                        child: Stack(
                                          children: [
                                            SvgPicture.string(StringUtility.getSvgForEvent(eventModels[x + 1].theme, eventModels[x + 1].themeColor1, eventModels[x + 1].themeColor2), height: 150),
                                            Positioned.fill(
                                              child: Align(
                                                alignment: Alignment.bottomLeft,
                                                child: Container(
                                                  padding: EdgeInsets.all(10),
                                                  child: Text(eventModels[x + 1].name, style: TextStyle(fontSize: 19, color: Colors.white, fontFamily: 'AirbnbCerealBlack')),
                                                ),
                                              ),
                                            ),
                                            Positioned.fill(
                                              child: Align(
                                                alignment: Alignment.topRight,
                                                child: Container(
                                                  padding: EdgeInsets.all(15),
                                                  child: CView.getEventIcon(eventModels[x + 1].icon, eventModels[x + 1].iconType),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      onTap: () {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => EventDetailesView(eventModel: eventModels[x+1])));
                                      },
                                    ),
                                ],
                              ),
                        ],
                      )
                    ]),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void getAllByUser(List<EventModel> _eventModels) {
    if (eventModels != null) {
      setState(() {
        eventModels = _eventModels;
      });
    }
  }

  @override
  void toastMessage(String message) {
    Fluttertoast.showToast(msg: message, toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM);
  }
}

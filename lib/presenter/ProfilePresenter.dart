import 'dart:ffi';

import 'package:mode_on/data/UsersApi.dart';
import 'package:mode_on/model/UsersModel.dart';
import 'package:mode_on/utility/StaticData.dart';

class ProfilePresenter {
  ProfileViewStateView _profileView;
  UsersApi _usersApi;

  ProfilePresenter(ProfileViewStateView profileView) {
    this._profileView = profileView;
    this._usersApi = new UsersApi();
  }

  Future<Void> currentUserInfo(String id) async {
    _profileView.loading(true);
    UsersModel usersModel = await _usersApi.getUserInfo(id);
    if (usersModel != null) {
      _profileView.currentUserInfo(usersModel);
    }
    _profileView.loading(false);
  }

  void changeProfileData(Map data, String id) async {
    await _usersApi.updateDocument(data, id);
  }

  void nameChange(String name) async {
    UsersModel usersModel = StaticData.CURRENT_USER;
    usersModel.fname = name;
    await _usersApi.updateDocument(usersModel.toJson(), usersModel.id);
  }
}

abstract class ProfileViewStateView {
  void isValidUser(bool isValid);

  void toastMessage(String message);

  void loading(bool isLoading);

  void currentUserInfo(UsersModel usersModel);
}

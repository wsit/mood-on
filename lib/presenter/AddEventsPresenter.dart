import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mode_on/data/EventsApi.dart';

class AddEventsPresenter {
  AddEventsPresenterView _addEventsPresenterView;
  EventsApi _eventsApi;

  AddEventsPresenter(AddEventsPresenterView addEventsPresenterView) {
    this._addEventsPresenterView = addEventsPresenterView;
    this._eventsApi = new EventsApi();
  }

  void save(Map data, String id) async {
    _addEventsPresenterView.loading(true);
    if (id == null) {
      DocumentReference newData = await _eventsApi.addDocument(data);
      _addEventsPresenterView.isSave(true);
    } else {
      await _eventsApi.updateDocument(data, id);
    }
    _addEventsPresenterView.loading(false);
  }
}

abstract class AddEventsPresenterView {
  void toastMessage(String message);

  void loading(bool isLoading);

  void isSave(bool isSave);
}

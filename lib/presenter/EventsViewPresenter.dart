import 'package:mode_on/data/EventsApi.dart';
import 'package:mode_on/model/EventModel.dart';

class EventsViewPresenter {
  EventsViewPresenterView _eventsViewPresenterView;
  EventsApi _eventsApi;

  EventsViewPresenter(EventsViewPresenterView eventsViewPresenterView) {
    this._eventsViewPresenterView = eventsViewPresenterView;
    this._eventsApi = new EventsApi();
  }

  void getAllByUser(String userId) async {
    List<EventModel> data = await _eventsApi.getData(userId);
    _eventsViewPresenterView.getAllByUser(data);
  }
}

abstract class EventsViewPresenterView {
  void getAllByUser(List<EventModel> eventModels);

  void toastMessage(String message);
}

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mode_on/model/EventModel.dart';
import 'package:mode_on/model/SurveyRate.dart';
import 'package:mode_on/presenter/HomeViewPresenter.dart';
import 'package:mode_on/utility/StaticData.dart';
import 'package:mode_on/utility/StringUtility.dart';
import 'package:mode_on/view/AddEventsView.dart';
import 'package:mode_on/view_custom/CView.dart';
import 'package:mode_on/view_custom/NavLayout.dart';
import 'package:mode_on/view_custom/ZoomScaffold.dart';
import 'package:provider/provider.dart';

import 'EventDetailes.dart';
import 'EventsView.dart';

class HomeView extends StatefulWidget {
  @override
  HomeViewState createState() => new HomeViewState();
}

class HomeViewState extends State<HomeView> with TickerProviderStateMixin, HomeViewPresenterView {
  MenuController menuController;
  HomeViewPresenter _homeViewPresenter;
  List<EventModel> _eventModels = new List<EventModel>();
  List<SurveyRate> surveyRate = new List<SurveyRate>();
  int myModeGraph = 2;

  @override
  void initState() {
    this.menuController = new MenuController(vsync: this);
    super.initState();
    _homeViewPresenter = new HomeViewPresenter(this);
    _homeViewPresenter.getFirstNRow(StaticData.CURRENT_USER.id, 4);
    _homeViewPresenter.getStatisticsByUser(StaticData.CURRENT_USER.id, myModeGraph == 3 ? 7 : myModeGraph == 2 ? 14 : 30);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: (context) => this.menuController,
      child: ZoomScaffold(
        menuScreen: NavLayout(),
        contentScreen: Layout(
          addBarAction: (cc) => Container(
            child: IconButton(onPressed: () {}, icon: Icon(Icons.nature, color: Colors.transparent)),
          ),
          contentBuilder: (cc) => Container(
            child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Colors.transparent,
              child: SingleChildScrollView(
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: new Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: Text("Good " + StringUtility.greeting(), textAlign: TextAlign.center, style: TextStyle(fontSize: 26, color: Color(0xff21233A), fontFamily: 'AirbnbCerealBold')),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Text(StaticData.CURRENT_USER.fname.split(" ")[0], textAlign: TextAlign.center, style: TextStyle(fontSize: 26, color: Color(0xff21233A), fontFamily: 'AirbnbCerealBold')),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 0.0, right: 0.0, top: 25.0),
                        child: new Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(child: Container(child: new Text('Events', style: TextStyle(fontSize: 20.0, color: Color(0xff718096), fontFamily: 'AirbnbCerealMedium'))), flex: 2),
                            Expanded(
                                child: Container(
                                  child: new GestureDetector(
                                    onTap: () {
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => EventsView()));
                                    },
                                    child: new Text(
                                      'see all',
                                      textAlign: TextAlign.right,
                                      style: TextStyle(fontSize: 18.0, color: Colors.grey, fontFamily: 'AirbnbCerealMedium'),
                                    ),
                                  ),
                                ),
                                flex: 2),
                          ],
                        ),
                      ),
                      new AnimatedContainer(
                        duration: const Duration(milliseconds: 800),
                        height: (_eventModels != null && _eventModels.length > 0) ? 200 : 10,
                        curve: Curves.fastOutSlowIn,
                        child: Container(
                          margin: EdgeInsets.symmetric(vertical: 20.0),
                          height: 150,
                          child: ListView(scrollDirection: Axis.horizontal, children: <Widget>[
                            for (EventModel eventModel in _eventModels)
                              InkWell(
                                child: Container(
                                  margin: EdgeInsets.only(right: 15),
                                  child: Stack(
                                    children: [
                                      SvgPicture.string(StringUtility.getSvgForEvent(eventModel.theme, eventModel.themeColor1, eventModel.themeColor2), height: 150),
                                      Positioned.fill(
                                        child: Align(
                                          alignment: Alignment.bottomLeft,
                                          child: Container(
                                            padding: EdgeInsets.only(bottom: 20, left: 15),
                                            child: Text(eventModel.name, style: TextStyle(fontSize: 18, color: Colors.white, fontFamily: 'AirbnbCerealBlack')),
                                          ),
                                        ),
                                      ),
                                      Positioned.fill(
                                        child: Align(
                                          alignment: Alignment.topRight,
                                          child: Container(
                                            padding: EdgeInsets.all(15),
                                            child: CView.getEventIcon(eventModel.icon, eventModel.iconType),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => EventDetailesView(eventModel: eventModel)));
                                },
                              ),
                          ]),
                        ),
                      ),
                      DottedBorder(
                        dashPattern: <double>[10, 7],
                        strokeWidth: 2.0,
                        color: Color(0xffA0AEC0),
                        borderType: BorderType.RRect,
                        radius: Radius.circular(20),
                        padding: EdgeInsets.all(6),
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(120)),
                          child: Container(
                              height: 70,
                              child: Align(
                                alignment: Alignment.center,
                                child: new GestureDetector(
                                  onTap: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => AddEventsView()));
                                  },
                                  child: Text(
                                    "+ Register Event",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 18, color: Color(0xffA0AEC0), fontFamily: 'AirbnbCerealMedium'),
                                  ),
                                ),
                              )),
                        ),
                      ),
                      InkWell(
                        child: Column(
                          children: [
                            /*Padding(
                              padding: EdgeInsets.only(left: 0.0, right: 0.0, top: 30.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(child: Container(child: new Text('See your mood', style: TextStyle(fontSize: 18.0, color: Color(0xff718096)))), flex: 2),
                                  Expanded(child: Container(child: new Text('>', textAlign: TextAlign.right, style: TextStyle(fontSize: 18.0, color: Color(0xff48609E)))), flex: 2),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                              child: Container(
                                height: 0.2,
                                color: Color(0xffA0AEC0),
                              ),
                            )*/
                          ],
                        ),
                        onTap: () {},
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 0.0, right: 0.0, top: 20.0),
                        child: new Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(child: Container(child: new Text('My Mood Graph', style: TextStyle(fontSize: 18.0, color: Color(0xff718096)))), flex: 2),
                            Expanded(
                                child: Container(
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      InkWell(
                                        child: new Text(
                                          ' Month ',
                                          textAlign: TextAlign.right,
                                          style: TextStyle(fontSize: myModeGraph == 1 ? 18 : 16, color: Color(0xff48609E), fontFamily: myModeGraph == 1 ? 'AirbnbCerealBold' : 'AirbnbCerealMedium'),
                                        ),
                                        onTap: () {
                                          setState(() {
                                            myModeGraph = 1;
                                          });
                                          _homeViewPresenter.getStatisticsByUser(StaticData.CURRENT_USER.id, 30);
                                        },
                                      ),
                                      InkWell(
                                        child: new Text(
                                          ' Week ',
                                          textAlign: TextAlign.right,
                                          style: TextStyle(fontSize: myModeGraph == 2 ? 18 : 16, color: Color(0xff48609E), fontFamily: myModeGraph == 2 ? 'AirbnbCerealBold' : 'AirbnbCerealMedium'),
                                        ),
                                        onTap: () {
                                          setState(() {
                                            myModeGraph = 2;
                                          });
                                          _homeViewPresenter.getStatisticsByUser(StaticData.CURRENT_USER.id, 14);
                                        },
                                      ),
                                      InkWell(
                                        child: new Text(
                                          ' Day ',
                                          textAlign: TextAlign.right,
                                          style: TextStyle(fontSize: myModeGraph == 3 ? 18 : 16, color: Color(0xff48609E), fontFamily: myModeGraph == 3 ? 'AirbnbCerealBold' : 'AirbnbCerealMedium'),
                                        ),
                                        onTap: () {
                                          setState(() {
                                            myModeGraph = 3;
                                          });
                                          _homeViewPresenter.getStatisticsByUser(StaticData.CURRENT_USER.id, 7);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                                flex: 2),
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 20)),
                      if (surveyRate != null && surveyRate.length > 0) CView.chart(context, surveyRate),
                    ]),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void getFirstNRow(List<EventModel> eventModels) {
    if (eventModels != null) {
      setState(() {
        _eventModels = eventModels;
      });
    }
  }

  @override
  void toastMessage(String message) {}

  @override
  void getStatistics(List<SurveyRate> data) {
    if (data != null) {
      setState(() {
        surveyRate = data;
      });
    }
  }
}

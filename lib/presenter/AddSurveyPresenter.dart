import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mode_on/data/SurveyApi.dart';

class AddSurveyPresenter {
  AddSurveyPresenterView _addSurveyPresenterView;
  SurveyApi _surveyApi;

  AddSurveyPresenter(AddSurveyPresenterView addSurveyPresenterView) {
    this._addSurveyPresenterView = addSurveyPresenterView;
    this._surveyApi = new SurveyApi();
  }

  void save(Map data, String id) async {
    _addSurveyPresenterView.loading(true);
    if (id == null) {
      DocumentReference newData = await _surveyApi.addDocument(data);
      _addSurveyPresenterView.isSave(true);
    } else {
      await _surveyApi.updateDocument(data, id);
    }
    _addSurveyPresenterView.loading(false);
  }
}

abstract class AddSurveyPresenterView {
  void toastMessage(String message);

  void loading(bool isLoading);

  void isSave(bool isSave);
}

import 'package:cloud_firestore/cloud_firestore.dart';

class SurveyModel {
  String id;
  int happy = 50;
  int content = 50;
  int relaxed = 50;
  String user = "";
  String event = "";
  Timestamp created = Timestamp.now();
  int created_int = Timestamp.now().millisecondsSinceEpoch;

  SurveyModel({this.id, this.happy, this.content, this.relaxed, this.user, this.event, this.created, this.created_int});

  SurveyModel.fromMap(Map snapshot, String id)
      : id = id ?? '',
        happy = snapshot['happy'] ?? 50,
        content = snapshot['content'] ?? 50,
        relaxed = snapshot['relaxed'] ?? 50,
        user = snapshot['user'] ?? "",
        event = snapshot['event'] ?? "",
        created = snapshot['created'] ?? null,
        created_int = snapshot['created_int'] ?? null;

  toJson() {
    return {
      "happy": happy,
      "content": content,
      "relaxed": relaxed,
      "event": event,
      "user": user,
      "created": created,
      "created_int": created_int,
    };
  }

  DateTime getDate(int created) {
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(created);
    return dateTime;
  }
}

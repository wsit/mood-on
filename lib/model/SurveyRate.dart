import 'dart:math';

import 'dart:ui';

class SurveyRate {
  int value = 0;
  String key = "";
  Color color = Color((new Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(0.5);

  SurveyRate(this.value, this.key);

  SurveyRate setColor(Color _color) {
    color = _color;
    return this;
  }
}

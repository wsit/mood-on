import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mode_on/model/UsersModel.dart';
import 'package:mode_on/presenter/ProfilePresenter.dart';
import 'package:mode_on/utility/LocalImage.dart';
import 'package:mode_on/utility/StaticData.dart';
import 'package:mode_on/utility/StringUtility.dart';
import 'package:mode_on/view_custom/NavLayout.dart';
import 'package:mode_on/view_custom/ZoomScaffold.dart';
import 'package:provider/provider.dart';

class ProfileView extends StatefulWidget {
  @override
  ProfileViewState createState() => new ProfileViewState();
}

class ProfileViewState extends State<ProfileView> with TickerProviderStateMixin implements ProfileViewStateView {
  ProfilePresenter _profilePresenter;
  MenuController menuController;
  String _userName = StaticData.CURRENT_USER.fname;
  String _userAddress = StaticData.CURRENT_USER.address;
  String _userEmail = StaticData.CURRENT_USER.email;
  String _userPhone = StaticData.CURRENT_USER.phone;
  String _userDob = StringUtility.humanDate(StaticData.CURRENT_USER.getDoB());
  String _userGender = StaticData.CURRENT_USER.gender;

  @override
  void initState() {
    this.menuController = new MenuController(vsync: this);
    super.initState();
    this._profilePresenter = ProfilePresenter(this);
    this._profilePresenter.currentUserInfo(StaticData.CURRENT_USER.id);
  }

  @override
  void dispose() {
    //menuController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: (context) => this.menuController,
      child: ZoomScaffold(
        menuScreen: NavLayout(),
        contentScreen: Layout(
          addBarAction: (cc) => Container(
            child: IconButton(onPressed: () {}, icon: Icon(Icons.nature, color: Colors.transparent)),
          ),
          contentBuilder: (cc) => Container(
            child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Colors.transparent,
              child: SingleChildScrollView(
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: new Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                      Stack(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 35, right: 40),
                            alignment: Alignment.center,
                            child: SvgPicture.string(
                                '<svg width="116" height="116" viewBox="0 0 116 116" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="116" height="116" rx="28" fill="#DA32D3"/></svg>'),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 0, left: 40),
                            alignment: Alignment.center,
                            child: SvgPicture.string(
                                '<svg width="116" height="116" viewBox="0 0 116 116" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="116" height="116" rx="28" fill="#48D8EC"/></svg>'),
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: new Column(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(top: 15),
                                  child: new Stack(fit: StackFit.loose, children: <Widget>[
                                    new Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        new Container(
                                          width: 116.0,
                                          height: 116.0,
                                          decoration: new BoxDecoration(
                                              shape: BoxShape.rectangle,
                                              borderRadius: new BorderRadius.all(Radius.circular(28)),
                                              image: new DecorationImage(image: new NetworkImage(StaticData.CURRENT_USER.image), fit: BoxFit.cover)),
                                        )
                                      ],
                                    ),
                                  ]),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 20)),
                      Center(child: Text(StaticData.CURRENT_USER.fname, textAlign: TextAlign.center, style: TextStyle(fontSize: 24, color: Color(0xff4A5568)))),
                      Padding(padding: EdgeInsets.only(top: 5)),
                      Center(child: Text("Total 12 events", textAlign: TextAlign.center, style: TextStyle(fontSize: 14, color: Colors.grey))),
                      Padding(padding: EdgeInsets.only(top: 20)),
                      Column(
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                child: Container(
                                  padding: EdgeInsets.all(15),
                                  width: 150,
                                  height: 90,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.1), spreadRadius: 5, blurRadius: 7, offset: Offset(0, 3))]),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Image.asset(LocalImage.iconUser, color: Colors.green, height: 20),
                                      Padding(padding: EdgeInsets.only(top: 10)),
                                      Text(_userName, textAlign: TextAlign.center, style: TextStyle(fontSize: 14, color: Color(0xff4A5568)))
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  changeProfileInfo(context, 'Name');
                                },
                              ),
                              Padding(padding: EdgeInsets.only(left: 10, right: 10)),
                              InkWell(
                                child: Container(
                                  padding: EdgeInsets.all(15),
                                  width: 150,
                                  height: 90,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.1), spreadRadius: 5, blurRadius: 7, offset: Offset(0, 3))]),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Icon(Icons.location_on, color: Colors.green, size: 20.0),
                                      Padding(padding: EdgeInsets.only(top: 10)),
                                      Text(_userAddress, textAlign: TextAlign.center, style: TextStyle(fontSize: 12, color: Color(0xff4A5568)))
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  changeProfileInfo(context, 'Address');
                                },
                              )
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 20)),
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                child: Container(
                                  padding: EdgeInsets.all(15),
                                  width: 150,
                                  height: 90,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.1), spreadRadius: 5, blurRadius: 7, offset: Offset(0, 3))]),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Icon(Icons.email, color: Colors.green, size: 20.0),
                                      Padding(padding: EdgeInsets.only(top: 10)),
                                      Text(_userEmail, textAlign: TextAlign.center, style: TextStyle(fontSize: 12, color: Color(0xff4A5568)))
                                    ],
                                  ),
                                ),
                                onTap: () {},
                              ),
                              Padding(padding: EdgeInsets.only(left: 10, right: 10)),
                              InkWell(
                                child: Container(
                                  padding: EdgeInsets.all(15),
                                  width: 150,
                                  height: 90,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.1), spreadRadius: 5, blurRadius: 7, offset: Offset(0, 3))]),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Icon(Icons.phone, color: Colors.green, size: 20.0),
                                      Padding(padding: EdgeInsets.only(top: 10)),
                                      Text(_userPhone, textAlign: TextAlign.center, style: TextStyle(fontSize: 12, color: Color(0xff4A5568)))
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  changeProfileInfo(context, 'Phone number');
                                },
                              )
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 20)),
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                child: Container(
                                  padding: EdgeInsets.all(15),
                                  width: 150,
                                  height: 90,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.1), spreadRadius: 5, blurRadius: 7, offset: Offset(0, 3))]),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Icon(Icons.calendar_today, color: Colors.green, size: 20.0),
                                      Padding(padding: EdgeInsets.only(top: 10)),
                                      Text(_userDob, textAlign: TextAlign.center, style: TextStyle(fontSize: 12, color: Color(0xff4A5568)))
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  changeProfileInfo(context, 'Date of birth');
                                },
                              ),
                              Padding(padding: EdgeInsets.only(left: 10, right: 10)),
                              InkWell(
                                child: Container(
                                  padding: EdgeInsets.all(15),
                                  width: 150,
                                  height: 90,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.1), spreadRadius: 5, blurRadius: 7, offset: Offset(0, 3))]),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Icon(Icons.nature_people, color: Colors.green, size: 20.0),
                                      Padding(padding: EdgeInsets.only(top: 10)),
                                      Text(_userGender, textAlign: TextAlign.center, style: TextStyle(fontSize: 14, color: Color(0xff4A5568)))
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  changeProfileInfo(context, 'Gender');
                                },
                              )
                            ],
                          ),
                        ],
                      )
                    ]),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void changeProfileInfo(context, String type) async {
    Widget _inputIcon = Icon(Icons.info, size: 24, color: Colors.grey);
    TextEditingController _controller = TextEditingController();
    int _inputType = 0;
    String _tempDob = null;
    if (type == 'Name') {
      _inputType = 0;
      _inputIcon = Icon(Icons.person, size: 24, color: Colors.grey);
    } else if (type == 'Phone number') {
      _inputType = 0;
      _inputIcon = Icon(Icons.phone, size: 24, color: Colors.grey);
    } else if (type == 'Address') {
      _inputType = 0;
      _inputIcon = Icon(Icons.location_on, size: 24, color: Colors.grey);
    } else if (type == 'Gender') {
      _inputType = 1;
      _inputIcon = Icon(Icons.nature_people, size: 24, color: Colors.grey);
    } else if (type == 'Date of birth') {
      _inputType = 2;
      _inputIcon = Icon(Icons.calendar_today, size: 24, color: Colors.grey);
    }

    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
      builder: (builder) {
        return StatefulBuilder(builder: (BuildContext context, StateSetter setState /*You can rename this!*/) {
          return new SingleChildScrollView(
            child: new Container(
              padding: EdgeInsets.only(top: 50, left: 20, right: 20,bottom: MediaQuery.of(context).viewInsets.bottom),
              decoration: new BoxDecoration(color: Colors.white, borderRadius: new BorderRadius.only(topLeft: const Radius.circular(50.0), topRight: const Radius.circular(50.0))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(child: Text("Change your " + (type).toLowerCase(), style: TextStyle(color: Color(0xff4A5568), fontSize: 18), textAlign: TextAlign.center), flex: 2),
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(top: 15)),
                  Text("$type", style: TextStyle(color: Color(0xff4A5568), fontSize: 18)),
                  Padding(padding: EdgeInsets.only(top: 15)),
                  if (_inputType == 0)
                    InkWell(
                      child: TextField(
                        controller: _controller,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          border: new OutlineInputBorder(borderSide: new BorderSide(color: Colors.grey)),
                          prefixIcon: _inputIcon,
                        ),
                      ),
                      onTap: () {},
                    ),
                  if (_inputType == 1)
                    Container(
                      height: 45,
                      width: double.infinity,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
                      child: DropdownButton<String>(
                        underline: SizedBox(),
                        isExpanded: true,
                        value: _userGender == "" ? "Select" : _userGender,
                        items: ['Select', 'Male', 'Female', 'Other'].toList().map((String item) => new DropdownMenuItem<String>(value: item, child: new Text(item))).toList(),
                        onChanged: (s) {
                          if (s == "Select") {
                            setState(() {
                              _userGender = "";
                            });
                          } else {
                            setState(() {
                              _userGender = s;
                            });
                          }
                        },
                      ),
                    ),
                  if (_inputType == 2)
                    Container(
                      height: 130,
                      width: double.infinity,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
                      child: CupertinoDatePicker(
                        mode: CupertinoDatePickerMode.date,
                        initialDateTime: StaticData.CURRENT_USER.getDoB() != null ? StaticData.CURRENT_USER.getDoB() : DateTime(1969, 1, 1),
                        onDateTimeChanged: (DateTime newDateTime) {
                          if (newDateTime != null) {
                            _tempDob = newDateTime.toString();
                          } else {
                            _tempDob = null;
                          }
                        },
                      ),
                    ),
                  Padding(padding: EdgeInsets.only(top: 20)),
                  Container(
                    width: double.infinity,
                    child: FlatButton(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                      color: Color(0xBB0FC2FA),
                      textColor: Colors.white,
                      splashColor: Colors.blueAccent,
                      padding: EdgeInsets.all(10),
                      child: Text("Save", style: TextStyle(color: Colors.white, fontSize: 18, fontFamily: "AirbnbCerealNormal")),
                      onPressed: () {
                        Navigator.of(context).pop();
                        if (type == 'Name') {
                          StaticData.CURRENT_USER.fname = _controller.text;
                          _profilePresenter.changeProfileData(StaticData.CURRENT_USER.toJson(), StaticData.CURRENT_USER.id);
                        } else if (type == 'Phone number') {
                          StaticData.CURRENT_USER.phone = _controller.text;
                          _profilePresenter.changeProfileData(StaticData.CURRENT_USER.toJson(), StaticData.CURRENT_USER.id);
                        } else if (type == 'Address') {
                          StaticData.CURRENT_USER.address = _controller.text;
                          _profilePresenter.changeProfileData(StaticData.CURRENT_USER.toJson(), StaticData.CURRENT_USER.id);
                        } else if (type == 'Gender') {
                          StaticData.CURRENT_USER.gender = _userGender;
                          _profilePresenter.changeProfileData(StaticData.CURRENT_USER.toJson(), StaticData.CURRENT_USER.id);
                        } else if (type == 'Date of birth') {
                          if (_tempDob != null) {
                            StaticData.CURRENT_USER.dob = _tempDob;
                            _profilePresenter.changeProfileData(StaticData.CURRENT_USER.toJson(), StaticData.CURRENT_USER.id);
                          }
                        }
                        currentUserInfo(StaticData.CURRENT_USER);
                      },
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 20)),
                ],
              ),
            ),
          );
        });
      },
    );

    if (type == 'Name') {
      _controller.text = StaticData.CURRENT_USER.fname;
    } else if (type == 'Phone number') {
      _controller.text = StaticData.CURRENT_USER.phone;
    } else if (type == 'Address') {
      _controller.text = StaticData.CURRENT_USER.address;
    } else if (type == 'Gender') {
      _controller.text = StaticData.CURRENT_USER.address;
    } else if (type == 'Date of birth') {
      _controller.text = StaticData.CURRENT_USER.address;
    }
  }

  @override
  void currentUserInfo(UsersModel usersModel) {
    setState(() {
      StaticData.CURRENT_USER = usersModel;
      _userName = StaticData.CURRENT_USER.fname;
      _userAddress = StaticData.CURRENT_USER.address;
      _userEmail = StaticData.CURRENT_USER.email;
      _userPhone = StaticData.CURRENT_USER.phone;
      _userDob = StringUtility.humanDate(StaticData.CURRENT_USER.getDoB());
      _userGender = StaticData.CURRENT_USER.gender;
    });
  }

  @override
  void isValidUser(bool isValid) {}

  @override
  void loading(bool isLoading) {}

  @override
  void toastMessage(String message) {}
}

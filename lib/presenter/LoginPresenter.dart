import 'dart:ffi';

import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:mode_on/data/UsersApi.dart';
import 'package:mode_on/model/UsersModel.dart';
import 'package:mode_on/utility/StaticData.dart';

class LoginAppPresenter {
  FirebaseAuth _auth;
  GoogleSignIn _googleSignIn;
  LoginAppView _loginAppView;
  UsersApi _usersApi;

  LoginAppPresenter(LoginAppView loginAppView) {
    this._loginAppView = loginAppView;
    this._auth = FirebaseAuth.instance;
    this._googleSignIn = GoogleSignIn();
    this._usersApi = new UsersApi();
  }

  Future<Void> signInWithGoogle() async {
    _loginAppView.loading(true);
    final GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;
    _loginAppView.toastMessage("Please wait! processing");
    final AuthResult authResult = await _auth.signInWithCredential(GoogleAuthProvider.getCredential(accessToken: googleSignInAuthentication.accessToken, idToken: googleSignInAuthentication.idToken));
    final FirebaseUser user = authResult.user;
    loginSuccess(user);
  }

  Future<Void> signInWithApple() async {
    _loginAppView.loading(true);
    final AuthorizationResult result = await AppleSignIn.performRequests([AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName])]);

    switch (result.status) {
      case AuthorizationStatus.authorized:
        await FlutterSecureStorage().write(key: "userId", value: result.credential.user);
        final AuthCredential credential = OAuthProvider(providerId: 'apple.com')
            .getCredential(idToken: String.fromCharCodes(result.credential.identityToken), accessToken: String.fromCharCodes(result.credential.authorizationCode));
        final AuthResult firebaseResult = await FirebaseAuth.instance.signInWithCredential(credential);
        final FirebaseUser user = firebaseResult.user;
        loginSuccess(user);
        break;
      case AuthorizationStatus.error:
        _loginAppView.toastMessage("Sign in failed 😿");
        break;
      case AuthorizationStatus.cancelled:
        _loginAppView.toastMessage("Cancelled");
        break;
    }
  }

  void loginSuccess(FirebaseUser user) async {
    UsersModel usersModel = await _usersApi.getUserInfo(user.uid);
    if (usersModel == null) {
      usersModel = new UsersModel();
      usersModel.fname = user.displayName;
      usersModel.lname = user.displayName;
      usersModel.email = user.email;
      usersModel.image = user.photoUrl;
      usersModel.gender = "Male";
      usersModel.phone = "";
      usersModel.address = "";
      await _usersApi.addDocumentById(usersModel.toJson(), user.uid);
    }
    usersModel.id = user.uid;
    StaticData.CURRENT_USER = usersModel;
    _loginAppView.isValidUser(true);
    _loginAppView.loading(false);
  }
}

abstract class LoginAppView {
  void isValidUser(bool isValid);

  void toastMessage(String message);

  void loading(bool isLoading);
}

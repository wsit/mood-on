import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mode_on/model/SurveyRate.dart';
import 'package:mode_on/utility/LocalImage.dart';

import '../view/LoginView.dart';

class CView {
  static void isValidUser(BuildContext context) async {
    Object user = await FirebaseAuth.instance.currentUser();
    if (user == null) {
      Navigator.push(context, MaterialPageRoute(builder: (context) => LoginView()));
    }
  }

  static void logout(BuildContext context) async {
    await FirebaseAuth.instance.signOut();
    Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute<Null>(builder: (context) => LoginView()), (Route<dynamic> route) => false);
  }

  static List<Color> getTheme(int index) {
    if (index == 0) {
      return [const Color(0xFFF3699A), const Color(0xFFE94D84)];
    } else if (index == 1) {
      return [const Color(0xFF3195FA), const Color(0xff3177fa)];
    } else if (index == 2) {
      return [const Color(0xFF575F89), const Color(0xFF6c718c)];
    } else if (index == 3) {
      return [const Color(0xFF7787FF), const Color(0xFF919eff)];
    } else if (index == 4) {
      return [const Color(0xFF65CD69), const Color(0xFF85d488)];
    } else if (index == 5) {
      return [const Color(0xFFFB9E14), const Color(0xFFffaf38)];
    } else if (index == 6) {
      return [const Color(0xFF009688), const Color(0xFF0eb3a3)];
    } else if (index == 7) {
      return [const Color(0xFFA42FB8), const Color(0xFFa64cb5)];
    } else {
      return [const Color(0xFFEA5186), const Color(0xFFc85187)];
    }
  }

  static IconData getIconData(int index) {
    if (index == 0) {
      return Icons.alarm;
    } else if (index == 1) {
      return Icons.add_call;
    } else if (index == 2) {
      return Icons.bookmark_border;
    } else if (index == 3) {
      return Icons.cloud;
    } else if (index == 4) {
      return Icons.local_airport;
    } else if (index == 5) {
      return Icons.copyright;
    } else if (index == 6) {
      return Icons.shop;
    } else if (index == 7) {
      return Icons.thumb_up;
    } else if (index == 8) {
      return Icons.headset;
    } else if (index == 9) {
      return Icons.mic;
    } else if (index == 10) {
      return Icons.fastfood;
    } else if (index == 11) {
      return Icons.hotel;
    } else if (index == 12) {
      return Icons.flag;
    } else if (index == 13) {
      return Icons.monetization_on;
    } else if (index == 14) {
      return Icons.people_outline;
    } else if (index == 15) {
      return Icons.functions;
    }
    return Icons.functions;
  }

  static Widget getEventIcon(String val, String type) {
    if (type == 'icon_data') {
      return Icon(getIconData(int.parse(val)), size: 20, color: Colors.white);
    } else {
      return Image(image: AssetImage(LocalImage.logo1), color: Colors.white, height: 20, alignment: Alignment.centerRight);
    }
  }

  static Widget chart(BuildContext context, List<SurveyRate> surveyRate) {
    return Container(
      padding: EdgeInsets.all(10),
        width: double.infinity,
        color: Color(0xffe3e3e3),
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(children: <Widget>[
                if (surveyRate == null)
                  Align(
                    alignment: Alignment.center,
                    child: Text("\nNothing yet to show", textAlign: TextAlign.center, style: TextStyle(fontSize: 18, color: Colors.grey, fontFamily: 'AirbnbCerealBook')),
                  ),
                if (surveyRate != null)
                  if (surveyRate.length == 0)
                    Align(
                      alignment: Alignment.center,
                      child: Text("\nNothing yet to show", textAlign: TextAlign.center, style: TextStyle(fontSize: 18, color: Colors.grey, fontFamily: 'AirbnbCerealBook')),
                    ),
                for (SurveyRate sRate in surveyRate)
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Column(
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            Container(height: 200, width: 50, decoration: new BoxDecoration(color: Colors.white, borderRadius: new BorderRadius.all(Radius.circular(5)))),
                            Positioned.fill(
                              child: Align(
                                alignment: Alignment.bottomCenter,
                                child:
                                    Container(height: sRate.value.toDouble() * 2, width: 50, decoration: new BoxDecoration(color: sRate.color, borderRadius: new BorderRadius.all(Radius.circular(5)))),
                              ),
                            ),
                          ],
                        ),
                        Text(sRate.key, style: TextStyle(color: Color(0xff48609E)))
                      ],
                    ),
                  ),
              ]),
            )
          ],
        ));
  }
}

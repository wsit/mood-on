import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mode_on/model/EventModel.dart';
import 'package:mode_on/model/SurveyModel.dart';
import 'package:mode_on/presenter/AddSurveyPresenter.dart';
import 'package:mode_on/utility/StaticData.dart';
import 'package:mode_on/view_custom/NavLayout.dart';
import 'package:mode_on/view_custom/ZoomScaffold.dart';
import 'package:provider/provider.dart';

class AddSurveyView extends StatefulWidget {
  final EventModel eventModel;

  AddSurveyView({Key key, @required this.eventModel}) : super(key: key);

  @override
  AddSurveyViewState createState() => new AddSurveyViewState(eventModel);
}

class AddSurveyViewState extends State<AddSurveyView> with TickerProviderStateMixin, AddSurveyPresenterView {
  EventModel eventModel;
  double _sadHappy = 40.0;
  double _content = 40.0;
  double _relaxed = 40.0;
  MenuController menuController;
  AddSurveyPresenter _addSurveyPresenter;

  AddSurveyViewState(EventModel _eventModel) {
    this.eventModel = _eventModel;
  }

  @override
  void initState() {
    super.initState();
    _addSurveyPresenter = new AddSurveyPresenter(this);
    this.menuController = new MenuController(vsync: this);
  }

  @override
  void dispose() {
    //menuController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: (context) => this.menuController,
      child: ZoomScaffold(
        menuScreen: NavLayout(),
        contentScreen: Layout(
          addBarAction: (cc) => Container(
            child: IconButton(onPressed: () {}, icon: Icon(Icons.nature, color: Colors.transparent)),
          ),
          contentBuilder: (cc) => Container(
            child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Colors.transparent,
              child: SingleChildScrollView(
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Align(
                            alignment: Alignment.center,
                            child: Text("Tell About", textAlign: TextAlign.center, style: TextStyle(fontSize: 26, color: Color(0xff21233A), fontFamily: 'AirbnbCerealBold'))),
                        Align(
                            alignment: Alignment.center,
                            child: Text("Your Feelings", textAlign: TextAlign.center, style: TextStyle(fontSize: 26, color: Color(0xff21233A), fontFamily: 'AirbnbCerealBold'))),
                        Padding(
                          padding: EdgeInsets.only(left: 0.0, right: 0.0, top: 25.0),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Expanded(child: Container(child: Text("Sad", style: TextStyle(color: Color(0xff4A5568), fontSize: 20, fontFamily: "AirbnbCerealBold"))), flex: 2),
                              Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(right: 25),
                                    child: new Text(
                                      'Happy',
                                      textAlign: TextAlign.right,
                                      style: TextStyle(color: Color(0xff4A5568), fontSize: 20, fontFamily: "AirbnbCerealBold"),
                                    ),
                                  ),
                                  flex: 2),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 10),
                          child: SliderTheme(
                            data: SliderTheme.of(context).copyWith(
                              activeTrackColor: Color(0xffff3d3a),
                              inactiveTrackColor: Color(0x8bff3d3a),
                              trackHeight: 20.0,
                              thumbShape: RoundedRectangleThumbShape(
                                thumbRadius: 12,
                                roundness: 1,
                                thickness: 10,
                              ),
                              thumbColor: Colors.white,
                              overlayColor: Colors.green,
                              overlayShape: RoundSliderOverlayShape(overlayRadius: 0),
                              activeTickMarkColor: Colors.transparent,
                              inactiveTickMarkColor: Colors.transparent,
                              valueIndicatorShape: RoundedRectangleThumbShape(),
                            ),
                            child: Slider(
                              value: _sadHappy,
                              min: 0,
                              max: 100,
                              divisions: 10,
                              label: '$_sadHappy',
                              onChanged: (value) {
                                setState(() {
                                  _sadHappy = value;
                                });
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 0.0, right: 0.0, top: 50.0),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Expanded(child: Container(child: Text("Discontent", style: TextStyle(color: Color(0xff4A5568), fontSize: 20, fontFamily: "AirbnbCerealBold"))), flex: 2),
                              Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(right: 25),
                                    child: new Text(
                                      'Content',
                                      textAlign: TextAlign.right,
                                      style: TextStyle(color: Color(0xff4A5568), fontSize: 20, fontFamily: "AirbnbCerealBold"),
                                    ),
                                  ),
                                  flex: 2),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 10),
                          child: SliderTheme(
                            data: SliderTheme.of(context).copyWith(
                              activeTrackColor: Color(0xff8bc34a),
                              inactiveTrackColor: Color(0x8b8bc34a),
                              trackHeight: 20.0,
                              thumbShape: RoundedRectangleThumbShape(
                                thumbRadius: 12,
                                roundness: 1,
                                thickness: 10,
                              ),
                              thumbColor: Colors.white,
                              overlayColor: Colors.green,
                              overlayShape: RoundSliderOverlayShape(overlayRadius: 0),
                              activeTickMarkColor: Colors.transparent,
                              inactiveTickMarkColor: Colors.transparent,
                              valueIndicatorShape: RoundedRectangleThumbShape(),
                            ),
                            child: Slider(
                              value: _content,
                              min: 0,
                              max: 100,
                              divisions: 10,
                              label: '$_content',
                              onChanged: (value) {
                                setState(() {
                                  _content = value;
                                });
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 0.0, right: 0.0, top: 50.0),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Expanded(child: Container(child: Text("Stressed", style: TextStyle(color: Color(0xff4A5568), fontSize: 20, fontFamily: "AirbnbCerealBold"))), flex: 2),
                              Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(right: 25),
                                    child: new Text(
                                      'Relaxed',
                                      textAlign: TextAlign.right,
                                      style: TextStyle(color: Color(0xff4A5568), fontSize: 20, fontFamily: "AirbnbCerealBold"),
                                    ),
                                  ),
                                  flex: 2),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 10),
                          child: SliderTheme(
                            data: SliderTheme.of(context).copyWith(
                              activeTrackColor: Color(0xff00bcd4),
                              inactiveTrackColor: Color(0x8b00bcd4),
                              trackHeight: 20.0,
                              thumbShape: RoundedRectangleThumbShape(
                                thumbRadius: 12,
                                roundness: 1,
                                thickness: 10,
                              ),
                              thumbColor: Colors.white,
                              overlayColor: Colors.green,
                              overlayShape: RoundSliderOverlayShape(overlayRadius: 0),
                              activeTickMarkColor: Colors.transparent,
                              inactiveTickMarkColor: Colors.transparent,
                              valueIndicatorShape: RoundedRectangleThumbShape(),
                            ),
                            child: Slider(
                              value: _relaxed,
                              min: 0,
                              max: 100,
                              divisions: 10,
                              label: '$_relaxed',
                              onChanged: (value) {
                                setState(() {
                                  _relaxed = value;
                                });
                              },
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(top: 50)),
                        Container(
                          padding: EdgeInsets.only(right: 20),
                          width: double.infinity,
                          child: FlatButton(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                            color: Color(0xBB0FC2FA),
                            textColor: Colors.white,
                            splashColor: Colors.blueAccent,
                            padding: EdgeInsets.all(10),
                            child: Text("Apply Survey", style: TextStyle(color: Colors.white, fontSize: 22, fontFamily: "AirbnbCerealNormal")),
                            onPressed: () {
                              SurveyModel sm = new SurveyModel();
                              sm.happy = _sadHappy.toInt();
                              sm.content = _content.toInt();
                              sm.relaxed = _relaxed.toInt();
                              sm.user = StaticData.CURRENT_USER.id;
                              sm.event = eventModel.id;
                              sm.created = Timestamp.now();
                              sm.created_int = Timestamp.now().millisecondsSinceEpoch;
                              _addSurveyPresenter.save(sm.toJson(), null);
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void isSave(bool isSave) {
    if (isSave) {
      toastMessage("Survey add successfully");
    } else {
      toastMessage("Survey add failed");
    }
  }

  @override
  void loading(bool isLoading) {}

  @override
  void toastMessage(String message) {
    Fluttertoast.showToast(msg: message, toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM);
  }
}

class RoundedRectangleThumbShape extends SliderComponentShape {
  final double thumbRadius;
  final double thickness;
  final double roundness;

  const RoundedRectangleThumbShape({this.thumbRadius = 6.0, this.thickness = 2, this.roundness = 6.0});

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    return Size.fromRadius(thumbRadius);
  }

  @override
  void paint(PaintingContext context, Offset center,
      {Animation<double> activationAnimation,
      Animation<double> enableAnimation,
      bool isDiscrete,
      TextPainter labelPainter,
      RenderBox parentBox,
      SliderThemeData sliderTheme,
      TextDirection textDirection,
      double value,
      double textScaleFactor,
      Size sizeWithOverflow}) {
    final Canvas canvas = context.canvas;

    final rect = Rect.fromCircle(center: center, radius: thumbRadius);

    final roundedRectangle1 = RRect.fromRectAndRadius(Rect.fromPoints(Offset(rect.left + 10, rect.top - 2), Offset(rect.right + 1, rect.bottom + 2)), Radius.circular(roundness));
    final borderPaint1 = Paint();
    borderPaint1.color = Color(0xffdadada);
    borderPaint1.strokeWidth = 2;
    borderPaint1.style = PaintingStyle.stroke;
    canvas.drawRRect(roundedRectangle1, borderPaint1);

    final roundedRectangle2 = RRect.fromRectAndRadius(Rect.fromPoints(Offset(rect.left + 11, rect.top - 1), Offset(rect.right, rect.bottom + 1)), Radius.circular(roundness));
    final fillPaint2 = Paint();
    fillPaint2.color = Colors.white;
    fillPaint2.style = PaintingStyle.fill;
    canvas.drawRRect(roundedRectangle2, fillPaint2);

    final borderPaint2 = Paint();
    borderPaint2.color = Colors.white;
    borderPaint2.strokeWidth = 3.0;
    borderPaint2.style = PaintingStyle.stroke;
    canvas.drawRRect(roundedRectangle2, borderPaint2);

    final roundedRectangle3 = RRect.fromRectAndRadius(Rect.fromPoints(Offset(rect.left + 15, rect.top + 5), Offset(rect.right - 10, rect.bottom - 5)), Radius.circular(0));
    canvas.drawRRect(roundedRectangle3, borderPaint1);

    final roundedRectangle4 = RRect.fromRectAndRadius(Rect.fromPoints(Offset(rect.left + 20, rect.top + 5), Offset(rect.right - 5, rect.bottom - 5)), Radius.circular(0));
    canvas.drawRRect(roundedRectangle4, borderPaint1);
  }
}

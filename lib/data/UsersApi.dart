import 'package:mode_on/model/UsersModel.dart';

import 'FirebaseApi.dart';

class UsersApi extends FirebaseApi {
  UsersApi() : super("mode-on-users");

  Future<UsersModel> getUserInfo(String id) async {
    final data = await getDocumentById(id);
    if (data.data != null) {
      return UsersModel.fromMap(data.data, data.documentID);
    } else {
      return null;
    }
  }
}

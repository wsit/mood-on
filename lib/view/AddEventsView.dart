import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mode_on/model/EventModel.dart';
import 'package:mode_on/presenter/AddEventsPresenter.dart';
import 'package:mode_on/utility/StaticData.dart';
import 'package:mode_on/view_custom/CView.dart';
import 'package:mode_on/view_custom/NavLayout.dart';
import 'package:mode_on/view_custom/ZoomScaffold.dart';
import 'package:provider/provider.dart';

class AddEventsView extends StatefulWidget {
  @override
  AddEventsViewState createState() => new AddEventsViewState();
}

class AddEventsViewState extends State<AddEventsView> with TickerProviderStateMixin, AddEventsPresenterView {
  int _selectedIcon = -1;
  int _selectedTheme = -1;
  MenuController menuController;
  AddEventsPresenter _addEventsPresenter;
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _addEventsPresenter = new AddEventsPresenter(this);
    menuController = new MenuController(
      vsync: this,
    )..addListener(() => setState(() {}));
  }

  @override
  void dispose() {
    //menuController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: (context) => menuController,
      child: ZoomScaffold(
        menuScreen: NavLayout(),
        contentScreen: Layout(
          addBarAction: (cc) => Container(
            padding: EdgeInsets.all(10),
            child: FlatButton(
              color: Color(0xff19DCB9),
              textColor: Colors.white,
              disabledColor: Colors.grey,
              disabledTextColor: Colors.black,
              splashColor: Colors.blueAccent,
              onPressed: () {
                List<Color> tempColors = CView.getTheme(_selectedTheme);
                if (_controller.text.length > 0) {
                  EventModel _eventModel = EventModel();
                  _eventModel.name = _controller.text;
                  _eventModel.icon = _selectedIcon.toString();
                  _eventModel.iconType = "icon_data";
                  _eventModel.themeColor1 = '#${tempColors[0].value.toRadixString(16)}';
                  _eventModel.themeColor2 = '#${tempColors[1].value.toRadixString(16)}';
                  _eventModel.user = StaticData.CURRENT_USER.id;
                  _eventModel.created = Timestamp.now();
                  _eventModel.theme = new Random().nextInt(5);
                  _addEventsPresenter.save(_eventModel.toJson(), null);
                } else {
                  toastMessage("Event Name is empty");
                }
              },
              child: Text("Done", style: TextStyle(fontSize: 16)),
            ),
          ),
          contentBuilder: (cc) => Container(
            child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Colors.transparent,
              child: SingleChildScrollView(
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: new Column(children: <Widget>[
                      Align(
                          alignment: Alignment.center,
                          child: Text("Create Your", textAlign: TextAlign.center, style: TextStyle(fontSize: 26, color: Color(0xff21233A), fontFamily: 'AirbnbCerealBold'))),
                      Align(
                          alignment: Alignment.center,
                          child: Text("Events Now", textAlign: TextAlign.center, style: TextStyle(fontSize: 26, color: Color(0xff21233A), fontFamily: 'AirbnbCerealBold'))),
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(top: 20)),
                            Text("Events Name", style: TextStyle(color: Colors.black)),
                            TextField(controller: _controller, decoration: InputDecoration(hintText: 'Events name')),
                            Padding(padding: EdgeInsets.only(top: 20)),
                            Text("Choose Icons", style: TextStyle(color: Colors.black)),
                            Container(
                              height: 100,
                              child: GridView.count(
                                crossAxisCount: 8,
                                children: List.generate(16, (index) {
                                  return InkWell(
                                    onTap: () {
                                      setState(() {
                                        if (_selectedIcon == index) {
                                          _selectedIcon = -1;
                                        } else {
                                          _selectedIcon = index;
                                        }
                                      });
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                          color: (index == _selectedIcon ? Color(0xffA0AEC0) : Colors.transparent),
                                          borderRadius: BorderRadius.all(Radius.circular(5)),
                                          boxShadow: [BoxShadow(color: Colors.transparent, spreadRadius: 5, blurRadius: 7, offset: Offset(0, 3))]),
                                      child: Icon(CView.getIconData(index), color: (_selectedIcon != index ? Color(0xff8F8F9C) : Colors.white), size: 24.0),
                                    ),
                                  );
                                }),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(top: 20)),
                            Text("Choose Theme", style: TextStyle(color: Colors.black)),
                            Container(
                              height: 800,
                              child: GridView.count(
                                crossAxisCount: 2,
                                children: List.generate(8, (index) {
                                  return Stack(
                                    children: [
                                      Container(
                                        padding: EdgeInsets.all(10),
                                        child: Container(
                                          decoration: new BoxDecoration(
                                            borderRadius: BorderRadius.all(Radius.circular(10)),
                                            gradient: new LinearGradient(
                                              colors: CView.getTheme(index),
                                              begin: const FractionalOffset(0.0, 0.0),
                                              end: const FractionalOffset(1.0, 1.0),
                                              stops: [0.0, 1.0],
                                              tileMode: TileMode.clamp,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Align(
                                        child: InkWell(
                                          child: Icon(Icons.check_circle, color: (_selectedTheme == index ? Colors.white : Colors.transparent), size: 70),
                                          onTap: () {
                                            setState(() {
                                              if (_selectedTheme == index) {
                                                _selectedTheme = -1;
                                              } else {
                                                _selectedTheme = index;
                                              }
                                            });
                                          },
                                        ),
                                      )
                                    ],
                                  );
                                }),
                              ),
                            ),
                          ],
                        ),
                      )
                    ]),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void loading(bool isLoading) {
    if (!isLoading) {
      toastMessage("Processing...");
    }
  }

  @override
  void toastMessage(String message) {
    Fluttertoast.showToast(msg: message, toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM);
  }

  @override
  void isSave(bool isSave) {
    setState(() {
      _controller.text = "";
      _selectedTheme = -1;
      _selectedIcon = -1;
    });
    toastMessage("Add successfully");
  }
}

import 'dart:ui';

import 'package:mode_on/model/UsersModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StaticData {
  static final String APP_NAME = "Mode On";
  static UsersModel CURRENT_USER = null;


  static const PrimaryColor =  Color(0xFF808080);
  static const PrimaryAssentColor =  Color(0xFF808080);
  static const PrimaryDarkColor =  Color(0xFF808080);
  //static const BackgroundColor =  Color(0xff232641);
  static const BackgroundColor =  Color(0xFFFAFAFA);

  static void setCookiesSting(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  static Future<String> getCookiesSting(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }
}

import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';
import 'package:mode_on/model/SurveyModel.dart';
import 'package:mode_on/model/SurveyRate.dart';

import 'FirebaseApi.dart';

class SurveyApi extends FirebaseApi {
  SurveyApi() : super("mode-on-survey");

  Future<List<SurveyModel>> getData(String eventId) async {
    List<SurveyModel> events = new List<SurveyModel>();
    QuerySnapshot data = await ref.where('event', isEqualTo: eventId).getDocuments();
    if (data.documents.length > 0) {
      for (int x = 0; x < data.documents.length; x++) {
        String id = data.documents[x].reference.path.split("/")[data.documents[x].reference.path.split("/").length - 1];
        events.add(SurveyModel.fromMap(data.documents[x].data, id));
      }
    }
    return events;
  }

  Future<List<SurveyRate>> getStatistics(String userId, String eventId, int day) async {
    String patter = "EEEE";
    if (day <= 7) {
      patter = "EEEE";
    }else if (day <= 60) {
      patter = "MMM d";
    }else if (day <= 180) {
      patter = "YYYY MMM d";
    }

    Map<String, double> hashMap = new HashMap<String, double>();
    QuerySnapshot data = null;
    if (userId != null && day > 0) {
      //data = await ref.where('user', isEqualTo: userId).getDocuments();
      data = await ref.where('user', isEqualTo: userId).where("created", isGreaterThan: (DateTime.now().subtract(new Duration(days: day)))).getDocuments();
    } else if (userId != null) {
      data = await ref.where('user', isEqualTo: userId).getDocuments();
    }
    if (eventId != null && day > 0) {
      //data = await ref.where("event", isEqualTo: eventId).getDocuments();
      data = await ref.where("event", isEqualTo: eventId).where("created", isGreaterThan: Timestamp.fromDate(DateTime.now().subtract(new Duration(days: day)))).getDocuments();
    } else if (eventId != null) {
      data = await ref.where("event", isEqualTo: eventId).getDocuments();
    }

    if (data != null && data.documents.length > 0) {
      for (int x = 0; x < data.documents.length; x++) {
        double val = (data.documents[x].data['happy'] + data.documents[x].data['content'] + data.documents[x].data['relaxed']) / 3;
        String key = DateFormat(patter).format(data.documents[x].data['created'].toDate());
        if (hashMap.containsKey(key)) {
          hashMap[key] = val + hashMap[key];
        } else {
          hashMap.putIfAbsent(key, () => val);
        }
      }
    }

    List<SurveyRate> srs = new List<SurveyRate>();
    for (String s in hashMap.keys) {
      srs.add(SurveyRate((hashMap[s]).toInt(), s));
    }

    if (day <= 7) {
    } else {
      srs.sort((a, b) {
        return a.key.toLowerCase().compareTo(b.key.toLowerCase());
      });
    }
    return srs;
  }
}

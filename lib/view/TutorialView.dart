import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mode_on/utility/LocalImage.dart';
import 'package:mode_on/utility/StaticData.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import 'LoginView.dart';

class TutorialView extends StatefulWidget {
  @override
  TutorialAppState createState() => new TutorialAppState();
}

class TutorialAppState extends State<TutorialView> with SingleTickerProviderStateMixin {
  final PageController controller = PageController(viewportFraction: 0.8);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(LocalImage.appBackground),
            fit: BoxFit.cover,
          ),
        ),
      ),
      Scaffold(
        backgroundColor: Colors.transparent,
        body: new Center(
          child: new ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.only(bottom: 40, top: 100, left: 70, right: 70),
                    height: MediaQuery.of(context).size.height * 0.6,
                    color: StaticData.BackgroundColor,
                    child: new Center(
                      child: Carousel(
                        showIndicator: false,
                        images: [
                          new Image(
                            image: AssetImage(LocalImage.iconTutorial1),
                            fit: BoxFit.fitWidth,
                            height: 350,
                          ),
                          AssetImage(LocalImage.iconTutorial2),
                          AssetImage(LocalImage.iconTutorial1)
                        ],
                        onImageChange: (x, y) {
                          setState(() {});
                        },
                      ),
                    ),
                  ),
                  Container(child: SmoothPageIndicator(controller: controller, count: 3, effect: WormEffect())),
                  Container(padding: EdgeInsets.only(top: 30)),
                  Text("SHARE YOUR EXPERIENCE", style: TextStyle(color: Colors.black, fontFamily: "AirbnbCerealBook", fontSize: 24)),
                  Container(padding: EdgeInsets.only(top: 10)),
                  Text("Today's your experience can build your future plan,", style: TextStyle(color: Colors.black, fontFamily: "AirbnbCerealBook", fontSize: 16)),
                  Text("Never get bored or too much excited", style: TextStyle(color: Colors.black, fontFamily: "AirbnbCerealBook", fontSize: 16)),
                  Container(
                    margin: EdgeInsets.only(top: 30, left: 20, right: 20),
                    color: Colors.transparent,
                    width: MediaQuery.of(context).size.width,
                    height: 60,
                    child: FlatButton(
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(5.0),
                      ),
                      onPressed: () {
                        StaticData.setCookiesSting("skip_tutorial", "true");
                        Navigator.push(context, MaterialPageRoute(builder: (context) => LoginView()));
                      },
                      color: Color(0xff46C5FB),
                      child: Text(
                        "LET’S GO",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'AirbnbCerealBlack',
                          fontSize: 22.0,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      )
    ]);
  }
}

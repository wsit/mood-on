import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mode_on/model/EventModel.dart';
import 'package:mode_on/model/SurveyModel.dart';
import 'package:mode_on/model/SurveyRate.dart';
import 'package:mode_on/presenter/EventDetailesViewPresenter.dart';
import 'package:mode_on/utility/LocalImage.dart';
import 'package:mode_on/utility/StringUtility.dart';
import 'package:mode_on/view_custom/CView.dart';
import 'package:mode_on/view_custom/NavLayout.dart';
import 'package:mode_on/view_custom/ZoomScaffold.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:provider/provider.dart';

import 'AddSurvey.dart';

class EventDetailesView extends StatefulWidget {
  final EventModel eventModel;

  EventDetailesView({Key key, @required this.eventModel}) : super(key: key);

  @override
  EventsViewState createState() => new EventsViewState(eventModel);
}

class EventsViewState extends State<EventDetailesView> with TickerProviderStateMixin, EventDetailesViewPresenterView {
  EventModel eventModel;
  bool _isBarChart = true;
  List<SurveyRate> surveyRate = new List<SurveyRate>();
  List<SurveyModel> _surveyModels = new List<SurveyModel>();
  EventDetailesViewPresenter _eventDetailesViewPresenter;

  double perHappy = 10, perContent = 20, perRelax = 30;

  EventsViewState(EventModel _eventModel) {
    this.eventModel = _eventModel;
  }

  MenuController menuController;

  @override
  void initState() {
    super.initState();
    this.menuController = new MenuController(vsync: this);
    this._eventDetailesViewPresenter = new EventDetailesViewPresenter(this);
    this._eventDetailesViewPresenter.getAllSurveyByEvent(eventModel.id);
    this._eventDetailesViewPresenter.getStatsSurveyByEvent(eventModel.id, 30);
  }

  @override
  void dispose() {
    //menuController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: (context) => this.menuController,
      child: ZoomScaffold(
        menuScreen: NavLayout(),
        contentScreen: Layout(
          addBarAction: (cc) => Container(
            padding: EdgeInsets.all(10),
            child: FlatButton(
              color: Color(0xff19DCB9),
              textColor: Colors.white,
              disabledColor: Colors.grey,
              disabledTextColor: Colors.black,
              splashColor: Colors.blueAccent,
              onPressed: () {
                _showMyDialog();
              },
              child: Text("+ Add Survey", style: TextStyle(fontSize: 16)),
            ),
          ),
          contentBuilder: (cc) => Container(
            child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Colors.transparent,
              child: SingleChildScrollView(
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: Text("Your Event", textAlign: TextAlign.center, style: TextStyle(fontSize: 26, color: Color(0xff21233A), fontFamily: 'AirbnbCerealBold')),
                      ),
                      Padding(padding: EdgeInsets.only(top: 20)),
                      Container(
                        padding: EdgeInsets.all(10),
                        margin: EdgeInsets.all(3),
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.1), spreadRadius: 5, blurRadius: 7, offset: Offset(0, 3))],
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            new Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    height: 40,
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        InkWell(
                                          child: Text("Bar Diagram", style: TextStyle(fontSize: 15.0, color: Color(0xff48609E), fontFamily: "AirbnbCerealMedium")),
                                          onTap: () {
                                            setState(() {
                                              _isBarChart = true;
                                            });
                                          },
                                        ),
                                        Container(
                                            height: 4,
                                            width: 30,
                                            decoration: new BoxDecoration(color: (_isBarChart ? Color(0xffFF9060) : Colors.transparent), borderRadius: new BorderRadius.all(Radius.circular(5))))
                                      ],
                                    ),
                                  ),
                                  flex: 2,
                                ),
                                Expanded(
                                  child: Container(
                                    height: 40,
                                    child: InkWell(
                                      child: new Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text("Pie Chart", style: TextStyle(fontSize: 15.0, color: Colors.grey, fontFamily: "AirbnbCerealMedium")),
                                          Container(
                                              height: 4,
                                              width: 30,
                                              decoration: new BoxDecoration(color: (_isBarChart ? Colors.transparent : Color(0xffFF9060)), borderRadius: new BorderRadius.all(Radius.circular(5))))
                                        ],
                                      ),
                                      onTap: () {
                                        setState(() {
                                          _isBarChart = false;
                                        });
                                      },
                                    ),
                                  ),
                                  flex: 2,
                                ),
                                Expanded(
                                    child: Container(
                                      height: 40,
                                      child: InkWell(
                                        child: Stack(
                                          alignment: Alignment.topRight,
                                          children: [
                                            Container(
                                              width: 40,
                                              height: 25,
                                              decoration: BoxDecoration(
                                                  color: Color(0xffDFECFC),
                                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                                  boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.1), spreadRadius: 5, blurRadius: 7, offset: Offset(0, 3))]),
                                              child: Icon(Icons.filter_list, color: Color(0xff598BC8), size: 22.0),
                                            )
                                          ],
                                        ),
                                        onTap: () {
                                          _showModalBottomSheet(context);
                                        },
                                      ),
                                    ),
                                    flex: 1),
                              ],
                            ),
                            _isBarChart
                                ? CView.chart(context, surveyRate)
                                : PieChart(
                                    dataMap: {'Happy': perHappy, 'Content': perContent, 'Relax': perRelax},
                                    colorList: [Colors.teal, Colors.blueAccent, Colors.amberAccent, Colors.redAccent],
                                    chartLegendSpacing: 32.0,
                                    chartRadius: MediaQuery.of(context).size.width / 2.7,
                                    showChartValuesInPercentage: true,
                                    legendPosition: LegendPosition.right,
                                    decimalPlaces: 1,
                                    chartType: ChartType.disc,
                                  ),
                          ],
                        ),
                      ),
                      for (SurveyModel sm in _surveyModels)
                        Container(
                          padding: EdgeInsets.all(15),
                          margin: EdgeInsets.only(top: 20, bottom: 10),
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.2), spreadRadius: 5, blurRadius: 7, offset: Offset(0, 3))],
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                height: 90,
                                child: new ListView(
                                  scrollDirection: Axis.horizontal,
                                  children: <Widget>[
                                    Container(
                                      width: MediaQuery.of(context).size.width * 0.6,
                                      child: ListView(
                                        children: [
                                          new Text(eventModel.name, textAlign: TextAlign.left, style: TextStyle(fontSize: 22.0, color: Color(0xff4A5568), fontFamily: 'AirbnbCerealBook')),
                                          new Text(StringUtility.humanDate(sm.created.toDate()),
                                              textAlign: TextAlign.left, style: TextStyle(fontSize: 14.0, color: Colors.grey, fontFamily: 'AirbnbCerealBook')),
                                          new Text('Happy: ' + sm.happy.toString() + '%    Relax: ' + sm.relaxed.toString() + '% ',
                                              textAlign: TextAlign.left, style: TextStyle(fontSize: 15.0, color: Color(0xff7D87FF), fontFamily: 'AirbnbCerealBook')),
                                          new Text('Content: ' + sm.content.toString() + '%',
                                              textAlign: TextAlign.left, style: TextStyle(fontSize: 15.0, color: Color(0xff7D87FF), fontFamily: 'AirbnbCerealBook')),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width * 0.2,
                                      alignment: Alignment.topRight,
                                      child: ListView(
                                        children: [
                                          Image(
                                            image: AssetImage((sm.happy + sm.relaxed) / 2 > 66 ? LocalImage.iconSmile : LocalImage.iconSad),
                                            height: 25,
                                            alignment: Alignment.centerRight,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                    ]),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Add Survey', style: TextStyle(color: Color(0xff4A5568), fontSize: 24, fontFamily: "AirbnbCerealBold")),
                Padding(padding: EdgeInsets.only(top: 10)),
                Text('Do you want to complement the event with a survey?', style: TextStyle(color: Color(0xff4A5568), fontSize: 20, fontFamily: "AirbnbCerealMedium")),
                Padding(padding: EdgeInsets.only(top: 35)),
                FlatButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                  color: Color(0xff0FC2FA),
                  textColor: Colors.white,
                  splashColor: Colors.blueAccent,
                  padding: EdgeInsets.all(10),
                  child: Text("Agree", style: TextStyle(color: Colors.white, fontSize: 22, fontFamily: "AirbnbCerealNormal")),
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).push(new CupertinoPageRoute<Null>(builder: (BuildContext context) => new AddSurveyView(eventModel: eventModel)));
                  },
                ),
                Padding(padding: EdgeInsets.only(top: 10)),
                FlatButton(
                  color: Colors.transparent,
                  textColor: Colors.white,
                  splashColor: Colors.black,
                  padding: EdgeInsets.all(10),
                  child: Text("Cancel", style: TextStyle(color: Colors.grey, fontSize: 22, fontFamily: "AirbnbCerealNormal")),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
          ),
        );
      },
    );
  }

  void _showModalBottomSheet(context) async {
    var _selectFilterOrder = 1;
    var _selectFilterOrderW = "";
    var _selectFilterOrderM = "";
    var _selectFilterOrderY = "";
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
      builder: (builder) {
        return StatefulBuilder(builder: (BuildContext context, StateSetter setState /*You can rename this!*/) {
          return new Container(
            child: new Container(
                padding: EdgeInsets.only(top: 50, left: 20, right: 20),
                decoration: new BoxDecoration(color: Colors.white, borderRadius: new BorderRadius.only(topLeft: const Radius.circular(50.0), topRight: const Radius.circular(50.0))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: InkWell(
                            child: Text("Close", style: TextStyle(color: Color(0xffA0AEC0), fontSize: 22, fontFamily: "AirbnbCerealBook"), textAlign: TextAlign.left),
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          flex: 2,
                        ),
                        Expanded(child: Text("Filter", style: TextStyle(color: Color(0xff4A5568), fontSize: 22, fontFamily: "AirbnbCerealBold"), textAlign: TextAlign.center), flex: 2),
                        Expanded(
                            child: InkWell(
                              child: Text("Reset", style: TextStyle(color: Color(0xffA0AEC0), fontSize: 22, fontFamily: "AirbnbCerealBook"), textAlign: TextAlign.right),
                              onTap: () {
                                setState(() {
                                  _selectFilterOrder = 1;
                                });
                              },
                            ),
                            flex: 2),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 20)),
                    Text("Listing orders", style: TextStyle(color: Color(0xff4A5568), fontSize: 22, fontFamily: "AirbnbCerealMedium")),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Container(
                            child: FlatButton(
                              onPressed: () {
                                setState(() {
                                  _selectFilterOrder = 1;
                                });
                              },
                              child: Text('Week', style: TextStyle(color: (_selectFilterOrder == 1 ? Colors.white : Color(0xBB0FC2FA)), fontSize: 18)),
                              color: (_selectFilterOrder == 1 ? Color(0xBB0FC2FA) : Colors.white),
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(color: (_selectFilterOrder == 1 ? Colors.white : Color(0xBB0FC2FA)), width: 1, style: BorderStyle.solid), borderRadius: BorderRadius.circular(5)),
                            ),
                            padding: EdgeInsets.only(right: 5),
                          ),
                          flex: 2,
                        ),
                        Expanded(
                          child: Container(
                            child: FlatButton(
                              onPressed: () {
                                setState(() {
                                  _selectFilterOrder = 2;
                                });
                              },
                              child: Text('Month', style: TextStyle(color: (_selectFilterOrder == 2 ? Colors.white : Color(0xBB0FC2FA)), fontSize: 18)),
                              color: (_selectFilterOrder == 2 ? Color(0xBB0FC2FA) : Colors.white),
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(color: (_selectFilterOrder == 2 ? Colors.white : Color(0xBB0FC2FA)), width: 1, style: BorderStyle.solid), borderRadius: BorderRadius.circular(5)),
                            ),
                            padding: EdgeInsets.only(right: 5),
                          ),
                          flex: 2,
                        ),
                        Expanded(
                          child: Container(
                            child: FlatButton(
                              onPressed: () {
                                setState(() {
                                  _selectFilterOrder = 3;
                                });
                              },
                              child: Text('Year', style: TextStyle(color: (_selectFilterOrder == 3 ? Colors.white : Color(0xBB0FC2FA)), fontSize: 18)),
                              color: (_selectFilterOrder == 3 ? Color(0xBB0FC2FA) : Colors.white),
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(color: (_selectFilterOrder == 3 ? Colors.white : Color(0xBB0FC2FA)), width: 1, style: BorderStyle.solid), borderRadius: BorderRadius.circular(5)),
                            ),
                            padding: EdgeInsets.only(right: 5),
                          ),
                          flex: 2,
                        )
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    Text("Dates", style: TextStyle(color: Color(0xff4A5568), fontSize: 22, fontFamily: "AirbnbCerealMedium")),
                    if (_selectFilterOrder == 1)
                      Container(
                        height: 45,
                        width: double.infinity,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
                        child: DropdownButton<String>(
                          underline: SizedBox(),
                          isExpanded: true,
                          value: _selectFilterOrderW == "" ? "Select Week" : _selectFilterOrderW,
                          items: ['Select Week', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'].toList().map((String item) => new DropdownMenuItem<String>(value: item, child: new Text(item))).toList(),
                          onChanged: (s) {
                            if (s == "Select Week") {
                              setState(() {
                                _selectFilterOrderW = "";
                              });
                            } else {
                              setState(() {
                                _selectFilterOrderW = s;
                              });
                            }
                          },
                        ),
                      ),
                    if (_selectFilterOrder == 2)
                      Container(
                        height: 45,
                        width: double.infinity,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
                        child: DropdownButton<String>(
                          underline: SizedBox(),
                          isExpanded: true,
                          value: _selectFilterOrderM == "" ? "Select Month" : _selectFilterOrderM,
                          items: ['Select Month', '1', '2', '3', '4', '5'].toList().map((String item) => new DropdownMenuItem<String>(value: item, child: new Text(item))).toList(),
                          onChanged: (s) {
                            if (s == "Select Month") {
                              setState(() {
                                _selectFilterOrderM = "";
                              });
                            } else {
                              setState(() {
                                _selectFilterOrderM = s;
                              });
                            }
                          },
                        ),
                      ),
                    if (_selectFilterOrder == 3)
                      Container(
                        height: 45,
                        width: double.infinity,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
                        child: DropdownButton<String>(
                          underline: SizedBox(),
                          isExpanded: true,
                          value: _selectFilterOrderY == "" ? "Select Year" : _selectFilterOrderY,
                          items: ['Select Year', '1', '2', '3'].toList().map((String item) => new DropdownMenuItem<String>(value: item, child: new Text(item))).toList(),
                          onChanged: (s) {
                            if (s == "Select Year") {
                              setState(() {
                                _selectFilterOrderY = "";
                              });
                            } else {
                              setState(() {
                                _selectFilterOrderY = s;
                              });
                            }
                          },
                        ),
                      ),
                    Padding(padding: EdgeInsets.only(top: 20)),
                    Container(
                      width: double.infinity,
                      child: FlatButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                        color: Color(0xBB0FC2FA),
                        textColor: Colors.white,
                        splashColor: Colors.blueAccent,
                        padding: EdgeInsets.all(10),
                        child: Text("Apply Filter", style: TextStyle(color: Colors.white, fontSize: 22, fontFamily: "AirbnbCerealNormal")),
                        onPressed: () {
                          Navigator.of(context).pop();
                          if (_selectFilterOrder == 1 && _selectFilterOrderW != "") {
                            this._eventDetailesViewPresenter.getStatsSurveyByEvent(eventModel.id, int.parse(_selectFilterOrderW) * 7);
                          } else if (_selectFilterOrder == 2 && _selectFilterOrderM != "") {
                            this._eventDetailesViewPresenter.getStatsSurveyByEvent(eventModel.id, int.parse(_selectFilterOrderM) * 30);
                          } else if (_selectFilterOrder == 3 && _selectFilterOrderY != "") {
                            this._eventDetailesViewPresenter.getStatsSurveyByEvent(eventModel.id, int.parse(_selectFilterOrderY) * 365);
                          }
                        },
                      ),
                    )
                  ],
                )),
          );
        });
      },
    );
  }

  @override
  void getData(List<SurveyModel> surveyModels) {
    double x = 0, y = 0, z = 0;
    if (surveyModels != null) {
      for (SurveyModel sm in surveyModels) {
        x += sm.happy;
        y += sm.content;
        z += sm.relaxed;
      }
      setState(() {
        _surveyModels = surveyModels;
        perHappy = x;
        perContent = y;
        perContent = z;
      });
    }
  }

  @override
  void toastMessage(String message) {}

  @override
  void getSurveyStatistics(List<SurveyRate> data) {
    if (data != null) {
      setState(() {
        surveyRate = data;
      });
    }
  }
}

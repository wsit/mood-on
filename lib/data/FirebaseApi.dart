import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseApi {
  final Firestore _db = Firestore.instance;
  final String path;
  CollectionReference ref;

  FirebaseApi(this.path) {
    ref = _db.collection(path);
  }

  Future<QuerySnapshot> getDataCollection() async {
    return await ref.getDocuments();
  }

  Future<DocumentSnapshot> getDocumentById(String id) async {
    return await ref.document(id).get();
  }

  Future<void> removeDocument(String id) async {
    return await ref.document(id).delete();
  }

  Future<DocumentReference> addDocument(Map data) async {
    return await ref.add(data);
  }

  Future<void> addDocumentById(Map data, String id) async {
    return await ref.document(id).setData(data);
  }

  Future<void> updateDocument(Map data, String id) async {
    return await ref.document(id).updateData(data);
  }
}

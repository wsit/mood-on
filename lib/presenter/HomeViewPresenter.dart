import 'package:mode_on/data/EventsApi.dart';
import 'package:mode_on/data/SurveyApi.dart';
import 'package:mode_on/model/EventModel.dart';
import 'package:mode_on/model/SurveyRate.dart';

class HomeViewPresenter {
  HomeViewPresenterView _homeViewPresenterView;
  EventsApi _eventsApi;
  SurveyApi _surveyApi;

  HomeViewPresenter(HomeViewPresenterView homeViewPresenterView) {
    this._homeViewPresenterView = homeViewPresenterView;
    this._eventsApi = new EventsApi();
    this._surveyApi = new SurveyApi();
  }

  void getFirstNRow(String userId, int row) async {
    List<EventModel> data = await _eventsApi.getDataNRow(userId, row);
    _homeViewPresenterView.getFirstNRow(data);
  }

  void getStatisticsByUser(String userId, int day) async {
    List<SurveyRate> data = await _surveyApi.getStatistics(userId, null, day);
    _homeViewPresenterView.getStatistics(data);
  }
}

abstract class HomeViewPresenterView {
  void getFirstNRow(List<EventModel> eventModels);

  void getStatistics(List<SurveyRate> data);

  void toastMessage(String message);
}

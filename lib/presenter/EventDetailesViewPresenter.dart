import 'package:mode_on/data/SurveyApi.dart';
import 'package:mode_on/model/SurveyModel.dart';
import 'package:mode_on/model/SurveyRate.dart';

class EventDetailesViewPresenter {
  EventDetailesViewPresenterView _eventDetailesViewPresenterView;
  SurveyApi _surveyApi;

  EventDetailesViewPresenter(EventDetailesViewPresenterView eventDetailesViewPresenterView) {
    this._eventDetailesViewPresenterView = eventDetailesViewPresenterView;
    this._surveyApi = new SurveyApi();
  }

  void getAllSurveyByEvent(String eventId) async {
    List<SurveyModel> data = await _surveyApi.getData(eventId);
    _eventDetailesViewPresenterView.getData(data);
  }

  void getStatsSurveyByEvent(String eventId, int day) async {
    List<SurveyRate> data = await _surveyApi.getStatistics(null, eventId, day);
    _eventDetailesViewPresenterView.getSurveyStatistics(data);
  }
}

abstract class EventDetailesViewPresenterView {
  void getData(List<SurveyModel> surveyModels);

  void getSurveyStatistics(List<SurveyRate> data);

  void toastMessage(String message);
}
